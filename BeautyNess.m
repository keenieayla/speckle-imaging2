% Scripts for nice plots
clear all


%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
%% ------------- DYNAMICS --------------
clear all

files = ["245msSpecklePar_spS1","245msSpeckleMed_spS1","245msSpeckleLar_spS1"];
type = ["brow", "ord","ord"]; 

for i = 1:1:length(files)   
    load(files(i));  
    maxLag = 1300;

    % The average temporal autocorrelation: 
    g2 = getG2(speckle,maxLag);
    
    % Save g2 function, together with T and specific cDisp
    filename = strcat('KAAg2',type(i),num2str(round(cDisp,4)));
    filename = strrep(filename,'.','');
    save(filename,'g2','T','cDisp');

end

%Plot g2 (Temporal Autocorrelation)
files = ["KAAg2brow00074","KAAg2ord0066","KAAg2ord0213"]; % all motions
figure(1)
for i = 1:1:length(files)
    load(files(i));    
    semilogx(g2,'LineWidth',2), hold on
end
xlabel('Time lag \tau, \mus')
ylabel('g_2(\tau)')
xlim([0 maxLag])
ylim([0.95 2])
set(gca,'units','points','Position',[50,50,350,250])
set(gcf, 'PaperUnits', 'centimeters');
set(gca,'FontSize',12)
grid on
legend('Parenchyma','Medium size vessel','Larger vessel','Location', 'northoutside','Orientation','horizontal')
exportgraphics(gca,'KAAg2Dynamics.png') 











%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
%% ------------- SPECKLE SIZES --------------
clear all

type='Par';name='Parenchyma';
expTime=5; %ms
maxLags=4;
spS=[0.1 1 2 3 4 10];
for idx = 1%:1:length(spS)
    spSize=spS(idx);
        if idx==1 
            fileToLoad=(sprintf('245msSpeckle%s_spS3',type));   
        else 
            fileToLoad=(sprintf('speckle%s_spS%.0f',type,spSize));         
        end  
    load(fileToLoad); 
    fImages(:,:,idx) = speckle(:,:,1);
    
    [estSpSize(idx),~] = getSpeckleSize(fImages(:,:,idx),maxLags)
    colormap gray
    set(gca,'FontSize',12)
    legend off
end    



%% ------------- SPECKLE SIZE GRAPH --------------
%% ------------- SPECKLE SIZE GRAPH --------------
%% ------------- SPECKLE SIZE GRAPH --------------
%% ------------- SPECKLE SIZE GRAPH --------------
%% ------------- SPECKLE SIZE GRAPH --------------
%% ------------- SPECKLE SIZE GRAPH --------------
%-------- simulation parameters -------- 
T = 1; dt = 1; 
pixelsN = 100; 

% constant for ordered motion in [medium large] vessel
load('tauFitbrow','qp');
cDisp = qp(1);

estPixSize = [16.2 8.038 5 4.5 4.2 4.1 3.9 3.62 3.5 3.4 3.3 3.2 3.1 3.0 2.9 2.8 2.7 2.6 2.55 2.525 2.5 2.475 2.45 2.4 2.3 2.2 2.1 2.0 1.906 1.55 1.25 0.817 0.606];

% Volume 
sXY = 100; sizeXY = ceil(estPixSize*sXY);

% Particles
pN = 1000; particlesN = ceil(pN.*(estPixSize.*estPixSize));
particleDensity = particlesN./(sizeXY.*sizeXY.*100); 

for idx=1:1:length(estPixSize)
    % generate speckle for estimated pixle size
    speckleEst(:,:,idx) = speckleSim('brownian',T,dt,sizeXY(idx),particlesN(idx),pixelsN,estPixSize(idx),cDisp);  
    [estSpSize(idx),~] = getSpeckleSize(speckleEst(:,:,idx),20); % compare to dmitry's corr
end 
save('KAAspeckleSizes2','speckleEst','T','cDisp','estPixSize','estSpSize','sizeXY','particlesN','particleDensity');

load('KAAspeckleSizes2.mat')
newlineSp=[estSpSize(3) 1.044 estSpSize(4)];
newlinePix=[estPixSize(3) 3.5 estPixSize(4)];

%speckleSize2 = (-0.02391)*PixelSize + 1.128;
%speckleSize1 = (-0.00442)*PixelSize + 1.058;
PixelSize = 2.5:2:20;
for idx = 1:1:length(PixelSize)
    speckleSize(idx) = (-0.02391)*PixelSize(idx) + 1.128;
end 

%%
%load('KAAspeckleSizes2.mat')
plotestPixSize=[estPixSize(1:2),estPixSize(8),estPixSize(21),estPixSize(29:33)];
plotestSpSize=[estSpSize(1:2),estSpSize(8),estSpSize(21),estSpSize(29:33)];   
% ----------------------
figure(1)
plot(plotestPixSize(4:9),plotestSpSize(4:9),'-k','LineWidth',2)
hold on
%plot(PixelSize,speckleSize,'--k','LineWidth',1)
plot(estPixSize(1:28),estSpSize(1:28),'x','Color',[0.5 0.5 0.5],'LineWidth',1,'MarkerSize',6)
%plot(plotestPixSize,plotestSpSize,'x','Color',[0.5 0.5 0.5],'LineWidth',1,'MarkerSize',6)
plot(plotestPixSize(4:8),plotestSpSize(4:8),'xr','LineWidth',2,'MarkerSize',8)
plot(plotestPixSize(4:8),plotestSpSize(4:8),'or','LineWidth',1,'MarkerSize',8)
plot(plotestPixSize(1:3),plotestSpSize(1:3),'xr','LineWidth',2,'MarkerSize',8)
%plot(plotestPixSize(1:3),plotestSpSize(1:3),'o','Color',[1 0.6 0.1],'LineWidth',1,'MarkerSize',8)
xlim([0.1,17])
ylim([0.1,10.8]) 
yticks([0:1:10])
xticks([1, 2.5, 5, 10, 15])
grid on
set(gca,'FontSize',12)
%set(gca,'xtick',[],'ytick',[])
set(gca,'units','points','Position',[50,50,350,250]) 
set(gcf, 'PaperUnits', 'centimeters');
xlabel( 'Pixel size [µm]', 'Interpreter', 'none');
ylabel( 'Speckle size [pixels]', 'Interpreter', 'none' );
exportgraphics(gca,sprintf('KAAspeckleSizeGraph.png'))  
% ----------------------
figure(2)
plot(plotestPixSize(4:9),plotestSpSize(4:9),'-k','LineWidth',2)
hold on
plot(estPixSize(1:28),estSpSize(1:28),'x','Color',[0.5 0.5 0.5],'LineWidth',1,'MarkerSize',6)
plot(plotestPixSize(4:8),plotestSpSize(4:8),'xr','LineWidth',2,'MarkerSize',8)
plot(plotestPixSize(4:8),plotestSpSize(4:8),'or','LineWidth',1,'MarkerSize',8)
plot(plotestPixSize(1:3),plotestSpSize(1:3),'xr','LineWidth',2,'MarkerSize',8)
xlim([1.5,5.5])
xticks([2, 2.5, 3, 3.5, 4, 4.5, 5])
ylim([0.8,2.2]) 
yticks([1, 1.5, 2])
grid on
grid minor 
set(gca,'FontSize',12)
set(gca,'units','points','Position',[50,50,250,110]) 
set(gcf, 'PaperUnits', 'centimeters');
xlabel(''); 
ylabel('');
exportgraphics(gca,sprintf('KAAspeckleSizeGraphZOOM.png'))  



%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------
%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------
%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------
%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------
%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------
%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------
%% ------------- SINGLE FRAME AND AVERAGE IMAGE --------------

type='Par';
expTime=5; %ms
maxLags=20;
spS=[0.1 1 2 3 4 10];

for idx = 4%1:1:length(spS)
    spSize=spS(idx);
        if idx==1 
            fileToLoad=(sprintf('speckle%s_spS01',type));   
         else 
            fileToLoad=(sprintf('speckle%s_spS%.0f',type,spSize));         
        end  
    load(fileToLoad); 
    
    figure(3)
    imagesc(speckle(:,:,100))
    colormap gray
    axis image 
    set(gca,'xtick',[],'ytick',[])
    set(gca,'units','points','Position',[50,50,230,230]) 
    set(gcf, 'PaperUnits', 'centimeters');
    set(gca,'FontSize',12)
    exportgraphics(gca,sprintf('KAA2specklePar1_spS%.0f.png',spSize)) 
    
    
    figure(4)
    avImage5ms = mean(speckle(:,:,1:(expTime*1000)),3);
    imagesc(avImage5ms(:,:))
    colormap gray
    axis image 
    set(gca,'xtick',[],'ytick',[])
    set(gca,'units','points','Position',[50,50,230,230]) 
    set(gcf, 'PaperUnits', 'centimeters');
    set(gca,'FontSize',12)
    exportgraphics(gca,sprintf('KAA2specklePar5000_spS%.0f.png',spSize)) 
end     































