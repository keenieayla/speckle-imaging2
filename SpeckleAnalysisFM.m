%% Clean up
close all
clear all
clc

%% Select files for g2 analysis
% caclulate the g2 function and saves it as a .mat-file

% choose files from which to calc g2
% files = ["speckleBrow100","speckleBrow150","speckleBrow200"];
files = ["speckleOrd5","speckleOrd10"];

% choose motion type for each file
% type = ["brow","brow","brow"]; % either "brow" or "ord"
type = ["ord","ord"];

for i = 1:1:length(files)
    
    load(files(i));
    
    % Temporal Autocorrelation
    maxLag = 1500;

    % The average temporal autocorrelation: 
    g2 = getG2(speckle,maxLag);
    
    % Save g2 function, together with T and specific cDisp
    save(strcat('g2',type(i),num2str(1/cDisp)),'g2','T','cDisp');

end

%% Plot g2
% plot g2 function on a semilog axis

figure(1)
% choose files to plot
% files = ["g2brow100","g2brow150","g2brow200"];
files = ["g2ord5","g2ord10"];
for i = 1:1:length(files)
    load(files(i));    
    semilogx(g2), hold on
end
xlabel('\tau')
ylabel('g2')
legend(files)

%% Fit model to g2

% choose files and motion type for each file
% files = ["g2brow100","g2brow150","g2brow200"];
files = ["g2ord5","g2ord10"];
% type = ["brow","brow","brow"]; % either "brow" or "ord"
type = ["ord","ord"];

for i = 1:1:length(files)
    load(files(i));
    switch type(i)
        case 'ord'
            Y=g2(1:find(g2<1,1,'first')); % cutting of for g2<1
            t=(0:length(Y)-1)'; % preparing t
            % model to fit for ordered motion
            ft = fittype( '1+beta*(exp(-(x/tauc)^2))^2 +C', 'independent', 'x', 'dependent', 'y' );            
        case 'brow'
            Y=g2(1:find(g2<1,1,'first'));
            t=(0:length(Y)-1)';
            % model to fit for brownian motion
            ft = fittype( '1+beta*(exp(-(x/tauc)^1))^2 +C', 'independent', 'x', 'dependent', 'y' );
    end
    % check if g2 decorrelates under one
    if isempty(Y)
        error("g2 does not decorrelate propperly!")
    end
    
    % fit options
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    opts.Lower = [0 0 1];
    opts.Robust = 'LAR';
    
    % Fit model to data.
    [fitG2, ~] = fit( t, Y, ft, opts );
    
    % Save to original g2 .mat file
    save(strcat('g2',type(i),num2str(1/cDisp)),'fitG2','-append');
end

%% Find model of tauC and cDisp
% fit model to tauC values for ordered or browian motion
% choose files

% files = ["g2brow100","g2brow150","g2brow200"];
files = ["g2ord5","g2ord10"];
% type = 'brow'; % choose either "brow" or "ord" - not looped!
type = 'ord';

% prepare variables
X =  NaN(length(files),1);
Y =  NaN(length(files),1);

% load and write values in variables
for i = 1:1:length(files)
    load(files(i));
    X(i) = cDisp;
    Y(i) = 1/ fitG2.tauc;
end

% fit either line or polynomial
switch type
    case 'brow'
        ft = fittype( 'a*x^2 + b*x + c','independent','x','dependent','y');
    case 'ord'
        ft = fittype( 'a*x + b','independent','x','dependent','y');
end

% fit options
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Robust = 'LAR';
   
% fit ans save model
[fitTau, ~] = fit( X, Y, ft, opts ); % 2nd output fitting param    
save(strcat('fit',type,'tau'),'fitTau','X','Y');

%% Results

clear all

% display results of a fit

% choose motion type - "brow" or "ord"
type = 'brow';

% specify query point - what value do we want for cDisp so we get 1/tauC ?

% qp = [0.066, 0.213]; % this is the best guess for ordered motion
qp = 0.0074; % this is the best guess for brownian motion

% load file
load(strcat('fit',type,'tau'));

% plot
figure(2)
plot(X,Y,'ok'), hold on
estTau = fitTau(qp); 
plot(qp,estTau,'*k')
align = ["left","right"];
for i = 1:length(qp) 
    text(qp(i),fitTau(qp(i)),sprintf("   [%.4f,%.4f]    ",...
        qp(i),fitTau(qp(i))),'Position',[1 0],'HorizontalAlignment',align(i))
end
plot(fitTau,'--k')
xlabel('cDisp')
ylabel('^{1}/_{tauC}')
legend('Data','Query Points',strcat(type,"Fit"),'location','northwest')
disp(fitTau(qp))
saveas(figure(2),strcat('tauFit',type,'.png'))
save(strcat('tauFit',type),'estTau','qp')

%% Calculate error
% Simulate brownian motion
%Time
T = 15000; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 

%Volume 
sizeXYZ = 50;% mum 

%Particles
particlesN = 500;

%Sensor
% Set sensor to be 100x100 pixels
pixelsN = 25;
% Set pixelSize close to 1, 2, 4, 6, 8, 10
pixelSize = 1; % (mu m)
% Optimal motion parameters (c value):    
load('tauFitbrow','qp');
cBrownian = qp;
load('tauFitord','qp');
cOrdered = qp; % constant for ordered motion in [medium large] vessel

% Temporal Autocorrelation
maxLag = 1500;

% Simulate brownian motion
tic
for c = 1:1:length(cBrownian)
    cDisp = cBrownian(c);
    speckle = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cBrownian(c));  
    filename = strrep(sprintf('speckleBrow_c%.3f',cDisp),'.','');
    save(filename,'speckle','T','cDisp');
    %g2
    g2 = getG2(speckle,maxLag);
    filename = strrep(sprintf('g2brow_c%.3f',cDisp),'.','');
    save(filename,'g2','T','cDisp');
    Y=g2(1:find(g2<1,1,'first')); % cutting of for g2<1
    t=(0:length(Y)-1)'; % preparing t
    if isempty(Y)
        error("g2 does not decorrelate propperly!")
    end
    ft = fittype( '1+beta*(exp(-(x/tauc)^1))^2 +C', 'independent', 'x', 'dependent', 'y' );
    % fit options
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    opts.Lower = [0 0 1];
    opts.Robust = 'LAR';
    
    % Fit model to data.
    [fitG2, ~] = fit( t, Y, ft, opts );
    tauC = fitG2.tauc;
    % Save to original g2 .mat file
    save(filename,'fitG2','tauC','-append');
end
toc
% Simulate ordered motion 
tic
for c = 1:1:length(cOrdered)
    cDisp = cOrdered(c);
    speckle = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrdered(c));
    save(strcat('speckleOrd',num2str(1/cOrdered(c))),'speckle','T','cDisp');
    filename = strrep(sprintf('speckleOrd_c%.3f',cDisp),'.','');
    save(filename,'speckle','T','cDisp');
    %g2
    g2 = getG2(speckle,maxLag);
    filename = strrep(sprintf('g2ord_c%.3f',cDisp),'.','');
    save(filename,'g2','T','cDisp');
    Y=g2(1:find(g2<1,1,'first')); % cutting of for g2<1
    t=(0:length(Y)-1)'; % preparing t
    if isempty(Y)
        error("g2 does not decorrelate propperly!")
    end
    ft = fittype( '1+beta*(exp(-(x/tauc)^2))^2 +C', 'independent', 'x', 'dependent', 'y' );
    % fit options
    opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
    opts.Display = 'Off';
    opts.Lower = [0 0 1];
    opts.Robust = 'LAR';
    
    % Fit model to data.
    [fitG2, ~] = fit( t, Y, ft, opts );
    tauC = fitG2.tauc;
    % Save to original g2 .mat file
    save(filename,'fitG2','tauC','-append');
end
toc

