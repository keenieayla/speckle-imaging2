clc
clear all
close all

%% Spatial (datapoints are taken from speckleContrastVsKernelTS.m)

load('SpatOptSpecPointsOrdM.mat');
speckle = DataToSave(:,1);
medium = DataToSave(:,2);
load('SpatOptSpecPointsOrdL.mat');
large = DataToSave(:,2);
load('SpatOptSpecPointsBrowP.mat');
brow = DataToSave(:,2);

large_ratio = large./speckle;
medium_ratio = medium./speckle;
brow_ratio = brow./speckle;

large_mean = mean(large_ratio);
medium_mean = mean(medium_ratio);
brow_mean = mean(brow_ratio);
all_mean = mean([large_ratio; medium_ratio; brow_ratio]);

large_std = std(large_ratio)./sqrt(length(speckle));
medium_std = std(medium_ratio)./sqrt(length(speckle));
brow_std = std(brow_ratio)./sqrt(length(speckle));
all_std = std([large_ratio; medium_ratio; brow_ratio])./sqrt(3*length(speckle));

save('spatial_mean_std.mat')

%% Temporal (datapoints are taken from TempSpeckleContrastVsKernelTS.m) - Temp calc are done directly in TempSpeckleContrastVsKernelTS.m

load('TempOptSpecPointsOrdM.mat');
speckle = DataToSave(:,1);
medium = DataToSave(:,2);
load('TempOptSpecPointsOrdL.mat');
large = DataToSave(:,2);
load('TempOptSpecPointsBrowP.mat');
brow = DataToSave(:,2);

large_ratio = large./speckle;
medium_ratio = medium./speckle;
brow_ratio = brow./speckle;

large_ratio_mean = mean(large_ratio);
medium_ratio_mean = mean(medium_ratio);
brow_ratio_mean = mean(brow_ratio);
all_mean = mean([large_ratio; medium_ratio; brow_ratio]);

large_ratio_std = std(large_ratio)./sqrt(length(speckle));
medium_ratio_std = std(medium_ratio)./sqrt(length(speckle));
brow_ratio_std = std(brow_ratio)./sqrt(length(speckle));
all_std = std([large_ratio; medium_ratio; brow_ratio])./sqrt(3*length(speckle));

save('temp_mean_std.mat')
