clear all; close all; clc;
%%
% choose long files
% files = ["245msSpeckleLar_spS0001","245msSpeckleLar_spS001", "245msSpeckleLar_spS1", "245msSpeckleLar_spS2", "245msSpeckleLar_spS3","245msSpeckleLar_spS4"]; type = "OrdL";
% files = ["245msSpeckleMed_spS0001","245msSpeckleMed_spS001", "245msSpeckleMed_spS1", "245msSpeckleMed_spS2", "245msSpeckleMed_spS3","245msSpeckleMed_spS4"]; type = "OrdM"; 
files = ["245msSpecklePar_spS0001","245msSpecklePar_spS001", "245msSpecklePar_spS1", "245msSpecklePar_spS2", "245msSpecklePar_spS3","245msSpecklePar_spS4"]; type = "BrowP";

% set exposure time
imageSize = size(matfile(files(1)), 'speckle');
splitSetting = [5000,0]; % define frames for exposure time and dead time. Note: sum must be equal to numberOfFrames
splitPeriod = sum(splitSetting);
splitCount = idivide(int32(imageSize(3)), int32(splitPeriod));

% check if split equals frames
if mod(imageSize(3), splitPeriod) ~= 0
    error("ERROR: see split settings"); 
end

longSplitsMean = zeros(imageSize(1),imageSize(2), splitCount, length(files)); % long splits are equal to exposure time
shortSplitsMean = zeros(imageSize(1),imageSize(2), splitCount, length(files)); % short splits are equal to dead time

% generate images with exposure time
for i = 1:1:length(files)
    
    load(files(i));
    
    masterCursor = 1;
    for j = 1:splitCount
        % Cropping long segments
        masterCursorEnd = masterCursor + splitSetting(1) - 1;
        longSplitsMean(:,:,j,i) = mean(speckle(:,:,masterCursor:masterCursorEnd),3);
        masterCursor = masterCursorEnd + 1;
        
        % Cropping short segments
        masterCursorEnd = masterCursor + splitSetting(2) - 1;
        shortSplitsMean(:,:,j,i) = mean(speckle(:,:,masterCursor:masterCursorEnd),3);
        masterCursor = masterCursorEnd + 1;      
    end   
end

% Saves averaged images according to motion type
save(strcat('exposureData',type,num2str(splitSetting(1))),'longSplitsMean','shortSplitsMean','splitSetting');

% Visualize all averaged images for set exposure times and varying speckle sizes
% figure()
% for i=1:size(longSplitsMean,4)    
%     for j=1:size(longSplitsMean,3)
%         subplot(size(longSplitsMean,3), size(longSplitsMean,4), i + (j-1) * size(longSplitsMean,4))
%         imagesc(longSplitsMean(:,:,j,i));
%         axis('square')
%         colormap gray
%     end
% end

% Visualize 5 images with set exposure time of each speckle size (0.5, 1, 3)
% figure()
% for i=1:size(longSplitsMean,4)    
%     for j=1:5
%         subplot(5, size(longSplitsMean,4), i + (j-1) * size(longSplitsMean,4))
%         imagesc(longSplitsMean(:,:,j,i));
%         axis('square')
%         colormap gray
%     end
% end

%%
clc; clear all; close all; 
%% Comments: long- or shortSplitsMean = (X,Y,images,files) and SplitsMean(:,:,:,1) = 3-D matrix

% files = "exposureDataOrdL5000"; type = "OrdL";
% files = "exposureDataOrdM5000"; type = "OrdM";
% files = "exposureDataBrowP5000"; type = "BrowP";

load(files);

% Get temporal contrast
kernel = 9; % temporal kernel size
tLSCI = zeros(3,1);

for i = 1:size(longSplitsMean,4)
    tLSCI(i) = mean(getTLSCI(longSplitsMean(:,:,:,i),kernel,'cpu','none'),[1,2,3]);
end

sps = [0.5 0.8 1 2 3 4]; %[0.5 1 3];

figure(1)
plot(sps,tLSCI,'-ko')
xlabel('Pixels per speckle')
ylabel('Temporal contrast (Kernel = 9)')
set(gca,'FontSize',12)

%% -----------------------------------------------------------------------------------------------------------
clc; clear all; close all; 

%% Estimate the optimal kernel size

% Choose files and fit with sortedTempData in section below
% files = "exposureDataOrdL5000"; type = "OrdL";
% files = "exposureDataOrdM5000"; type = "OrdM";
files = "exposureDataBrowP5000"; type = "BrowP";

load(files);

kernelSize = 1:2:49;
tLSCI = zeros(size(longSplitsMean,4),length(kernelSize));

figure()
for i = 1:size(longSplitsMean,4)
    for j = 1:length(kernelSize)
        tLSCI(i,j) = mean(getTLSCI(longSplitsMean(:,:,:,i),kernelSize(j),'cpu','none'),[1,2,3]); % image size is changed to remove vignetting effect for contrast est.
    end

end
plot(kernelSize,tLSCI,'-o')
axis tight
grid on
legend('Speckle to pixel size ratio 0.5','Speckle to pixel size ratio 1','Speckle to pixel size ratio 3','Location','best')
ylabel('Temporal contrast')
xlabel('Kernel size')
set(gca,'Xtick',3:2:49,'FontSize',12)

% Find optimal point <------------------------------

contrast = tLSCI;
nPoints = length(kernelSize(1:13));
for i = 1:size(contrast,1)
    allCoord = [kernelSize;contrast(i,:)]';
    allCoord = allCoord(1:13,:); % <---- Use data for kernel 3..25

    % Pull out first point
    firstPoint = allCoord(1,:);

    % Create vector between first and last point
    lineVec = allCoord(end,:) - firstPoint;

    % Normalize vector
    lineVecN = lineVec / sqrt(sum(lineVec.^2));

    % Find the distance from each point to the line, by splitting data into 
    % perpendicular to the line and transversal.
    vecFromFirst = bsxfun(@minus, allCoord, firstPoint);

    scalarProduct = dot(vecFromFirst, repmat(lineVecN,nPoints,1), 2);
    vecFromFirstParallel = scalarProduct * lineVecN;
    vecToLine = vecFromFirst - vecFromFirstParallel;

    % Distance to line is the norm of vecToLine
    distToLine = sqrt(sum(vecToLine.^2,2));

    % Plot the distance to the line
    % figure('Name','distance from curve to line'), plot(distToLine)

    %Find the maximum aka the best trade off point
    [maxDist,idxOfBestPoint] = max(distToLine);

    idxOfBestPointAll(i) = idxOfBestPoint;
    idxOfBestPointAll(i) = idxOfBestPoint;
end


% %% Plot contrast vs. kernel size with optimal kernel points
% figure(3)
% plot(kernelSize,contrast(:,:))
% hold on
% for i = 1:size(contrast,1)
%     allCoord = [kernelSize;contrast(i,:)]';
%     plot(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), 'or','HandleVisibility','off') 
%     textString = sprintf('  [%.2f, %.2f]', allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2));
%     text(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), textString, 'FontSize', 10, 'Color', 'k');
% end
% legend('Speckle to pixel size ratio 0.5','Speckle to pixel size ratio 1','Speckle to pixel size ratio 3','Location','best')
% ylabel('Temporal contrast')
% xlabel('Kernel size')
% title('Optimal kernelsize for temporal contrast')
% set(gca,'Xtick',3:2:49,'FontSize',12)
% axis tight
% grid on
% saveas(figure(3),strcat('Temp_Contrast_vs_Kernel_size_',type,'Optimal.png'))

DataToSave2 = [kernelSize;contrast(1,:);contrast(2,:);contrast(3,:);contrast(4,:);contrast(5,:);contrast(6,:)];
save( strcat('TempDataToPlot',type,'.mat'),'DataToSave2');


spec = [0.5 0.8 1 2 3 4]; spec = spec'; 
DataToSave = [spec, allCoord(idxOfBestPointAll(:),1)];
% Manual overwriting outlier
DataToSave(4,2) = 5;
save( strcat('TempOptSpecPoints',type,'.mat'),'DataToSave');

%% Kernel analysis continued
% Note: fit type from section above with sortedTempData and run + save for
% all

% calculate kernel to speckle size ratio
spec = [0.5 0.8 1 2 3 4];
kernRatio2 = zeros(size(tLSCI));
for j=1:1:size(spec,2)
    kernRatio2(j,:) = (kernelSize)/spec(j);
end 
kern = kernRatio2'; kern = kern(:)';
con = tLSCI'; con = con(:)'; rcon = con./max(con);

% Choose type

% sortedTempDataOrdM = sortrows(([con ; kern]'),2);
% sortedTempDataOrdL = sortrows(([con ; kern]'),2);
% sortedTempDataBrowP = sortrows(([con ; kern]'),2);

% save('sortedTempData.mat','sortedTempDataOrdM','sortedTempDataOrdL','sortedTempDataBrowP')



%% Result of kernel analysis - RUN THIS WHEN ALL OTHER SECTIONS ARE RUN USING ALL 3 DATATYPES!
clc; clear all; close all; 

col = ['k' 'r' 'b' 'g' 'm' 'y' 'c'];
spec = [0.5 0.8 1 2 3 4];

figure(2)
subplot(1,3,1)
load('TempOptSpecPointsBrowP.mat');
load('TempDataToPlotBrowP.mat');
for i = 1:length(spec)
plot(DataToSave2(1,:),DataToSave2(i+1,:), 'LineWidth',2)
hold on
end
brow_mean = mean(DataToSave(:,2));
brow_std = std(DataToSave(:,2))./sqrt(length(DataToSave(:,2)));
plot([brow_mean brow_mean],[0 1], 'k', 'LineWidth',3)
h=fill([brow_mean-brow_std,brow_mean+brow_std,brow_mean+brow_std,brow_mean-brow_std],[0,0,1,1],'k');
h.FaceAlpha=0.3;
xlim([1 25])
ylim([0 0.4])
grid on
set(gca,'Xtick',1:4:50,'FontSize',16)
ylabel('Temporal contrast')
xlabel('Kernel size')
% title(['Brow. Opt. kernel/speckle = ' num2str(brow_mean,'%4.1f')])
hold off
str = ['Opt. kernel size ' num2str(brow_mean,'%4.1f')];
str2 = ['Std. of mean ' num2str(brow_std,'%4.1f')];
% hleg = legend('Speckle << 1','Speckle < 1','Speckle 1','Speckle 2','Speckle 3','Speckle 4',str, str2, 'Location', 'northeast','FontSize',10);
% htitle = get(hleg,'Title');
% set(htitle,'String','Parenchyma')

% Plot for Ord M
subplot(1,3,2)
load('TempOptSpecPointsOrdM.mat');
load('TempDataToPlotOrdM.mat');
for i = 1:length(spec)
plot(DataToSave2(1,:),DataToSave2(i+1,:), 'LineWidth',2)
hold on
end
medium_mean = mean(DataToSave(:,2));
medium_std = std(DataToSave(:,2))./sqrt(length(DataToSave(:,2)));
plot([medium_mean medium_mean],[0 0.35], 'k', 'LineWidth',3)
h=fill([medium_mean-medium_std,medium_mean+medium_std,medium_mean+medium_std,medium_mean-medium_std],[0,0,0.35,0.35],'k');
h.FaceAlpha=0.3;
xlim([1 25])
ylim([0 0.2])
grid on
set(gca,'Xtick',1:4:50,'FontSize',16)
% ylabel('Temporal contrast')
xlabel('Kernel size')
% title(['Brow. Opt. kernel/speckle = ' num2str(brow_mean,'%4.1f')])
hold off
str = ['Opt. kernel size ' num2str(brow_mean,'%4.1f')];
str2 = ['Std. of mean ' num2str(brow_std,'%4.1f')];
% hleg = legend('Speckle << 1','Speckle < 1','Speckle 1','Speckle 2','Speckle 3','Speckle 4',str, str2, 'Location', 'northeast','FontSize',10);
% htitle = get(hleg,'Title');
% set(htitle,'String','Medium')

% Plot for OrdL
subplot(1,3,3)
load('TempOptSpecPointsOrdL.mat');
load('TempDataToPlotOrdL.mat');
for i = 1:length(spec)
plot(DataToSave2(1,:),DataToSave2(i+1,:), 'LineWidth',2)
hold on
end
large_mean = mean(DataToSave(:,2));
large_std = std(DataToSave(:,2))./sqrt(length(DataToSave(:,2)));
plot([large_mean large_mean],[0 0.35], 'k', 'LineWidth',3)
h=fill([large_mean-large_std,large_mean+large_std,large_mean+large_std,large_mean-large_std],[0,0,0.35,0.35],'k');
h.FaceAlpha=0.3;
xlim([1 25])
ylim([0 0.1])
grid on
set(gca,'Xtick',1:4:50,'FontSize',16)
% ylabel('Temporal contrast')
xlabel('Kernel size')
% title(['Brow. Opt. kernel/speckle = ' num2str(brow_mean,'%4.1f')])
hold off
str = ['Opt. kernel size ' num2str(brow_mean,'%4.1f')];
str2 = ['Std. of mean ' num2str(brow_std,'%4.1f')];
% hleg = legend('Speckle << 1','Speckle < 1','Speckle 1','Speckle 2','Speckle 3','Speckle 4',str, str2, 'Location', 'northeast','FontSize',10);
% htitle = get(hleg,'Title');
% set(htitle,'String','Large')

set(gcf, 'Position',  [100,100,1500,500])
saveas(gcf,'temp_results.png')

figure(3)
for i = 1:length(spec)
plot(DataToSave2(1,:),DataToSave2(i+1,:), 'LineWidth',2)
hold on
end


plot([large_mean large_mean],[0 0.35], 'k', 'LineWidth',3)
h=fill([large_mean-large_std,large_mean+large_std,large_mean+large_std,large_mean-large_std],[0,0,0.35,0.35],'k');
h.FaceAlpha=0.3;

str = ['Opt. kernel size [' num2str(brow_mean,'%4.1f') ', ' num2str(medium_mean,'%4.1f') ', ' num2str(large_mean,'%4.1f') ']'];
str2 = ['Std. of mean [' num2str(brow_std,'%4.1f') ', ' num2str(medium_std,'%4.1f') ', ' num2str(large_std,'%4.1f') ']'];
legend('Speckle << 1','Speckle < 1','Speckle 1','Speckle 2','Speckle 3','Speckle 4', str, str2, 'Location', 'northoutside','orientation','horizontal','FontSize',12)

set(gcf, 'Position',  [100,100,1500,500])
saveas(gcf,'temp_results_legend.png')