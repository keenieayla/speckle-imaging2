% Estimate pixel sizes, to get speckle to pixel ratios 

%% ------------- BEGIN CODE --------------
clear all
close all
%-------- simulation parameters -------- 
% Time
T = 1; 
dt = 1; 
% Volume 
sizeXY = 100;% mum 
% Particles
particlesN = 1000;
% Sensor
pixelsN = 100; % 100x100 pixels, in theory we would like even larger

load('tauFitord','qp');
cDisp = qp(1);

% Set different pixel sizes and evaluate the speckle to pixel size ratio. 
pixelSize = [0.5:0.02:1.5,2:1:7]; % more needed in lower pixel sizes!

%% Simulate speckle patteren for different pixel sizes 

for idx = 1:1:length(pixelSize)
    pix = pixelSize(idx);
    speckle(:,:,idx) = speckleSim('ordered',T,dt,sizeXY,particlesN,pixelsN,pixelSize(idx),cDisp);  
end

%% Speckle size estimation
maxLags = 20; % the max expected width of a speckle, see SpecklePatternVisualization for 0.5

% estimate speckle size for different pixel sizes 
sSize = NaN(length(pixelSize),1);
for idx = 1:1:length(pixelSize)
    pix = pixelSize(idx);
    [sSize(idx),corr] = getSpeckleSize(speckle(:,:,idx),maxLags); % compare to dmitry's corr
end 

%% Fitting the curve, to get the pixel sizes for ratios 
[xData, yData] = prepareCurveData( pixelSize, sSize);
ft = fittype( 'linearinterp' );

% Fit model to data.
[fitSp2Pix, ~] = fit( xData, yData, ft);

x = [0.5:0.001:10]';
y = fitSp2Pix(x);
reqSpSize = [1 2 3 4 6 8];
estPixSize = NaN(length(reqSpSize),1);
for idx = 1:length(reqSpSize)
    estPixSize(idx) = x(find(y<=reqSpSize(idx),1));
end
%estPixSize10=estPixSize(6); % = 0.6060
estPixSize(7) = 0.6060; % for reqSpSize=10
reqSpSize = [1 2 3 4 6 8 10];

%% Plot fit with data

estPixSize(3) = 2.5; % for reqSpSize=3, adjusted since the error was quite large with previous value

figure(1) % speckle to pixel size
plot(x,y,'--k', estPixSize,reqSpSize,'*k')
for idx = 1:length(estPixSize) 
    text(estPixSize(idx),reqSpSize(idx)+0.3,sprintf("  [%.2f,%.1f]    ",...
        estPixSize(idx),reqSpSize(idx)),'Position',[1 0],'HorizontalAlignment','left')
end
xlim([0,10])
legend( 'Speckle size vs. Pixel size', 'Estimated Ratios', 'Location', 'NorthEast', 'Interpreter', 'none' );
xlabel( 'Pixel size [mum]', 'Interpreter', 'none' );
ylabel( 'Speckle size [pixels]', 'Interpreter', 'none' );
grid on

%% Check if it is correct

% generate speckle for estimated pixle size
for idx = 1:1:length(estPixSize)
    speckleEst(:,:,idx) = speckleSim('ordered',T,dt,sizeXY,particlesN,pixelsN,estPixSize(idx),cDisp);  
end

maxLags = 20; % the max expected width of a speckle, see SpecklePatternVisualization for 0.5

% estimate speckle size for estimated pixle size
estSpSize = NaN(length(estPixSize),1);
for idx = 1:1:length(estPixSize)
    [estSpSize(idx),~] = getSpeckleSize(speckleEst(:,:,idx),maxLags); % compare to dmitry's corr
end

% calculate the error
err = sqrt((estSpSize - reqSpSize').^2);

figure(2) % view speckles with required speckle size
for i = 1:1:length(estSpSize)
    subplot(2,4,i)
    imagesc(speckleEst(:,:,i)) 
    title(sprintf('Speckle Size: %.2f',estSpSize(i)))
    colormap gray
    axis image
end

% plot for paper
% figure()
% imagesc(speckleEst(:,:,3)) 
% colormap gray
% axis image
% set(gca,'xtick',[],'ytick',[])

%% Save, this is all we need to save!

filename = 'sp2pixSize';
save(filename,'speckleEst','reqSpSize','estSpSize','estPixSize','fitSp2Pix','err');
saveas(figure(1),strcat(filename,'.png'));
saveas(figure(2),strcat(filename,'Speckles','.png'));




%% ------------- END CODE --------------