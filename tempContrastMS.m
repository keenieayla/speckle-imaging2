
% Calculate exposure time effects
% -----------------   Start here   -----------------   %

% choose files
% files = ["speckleOrd_c0066_spS1", "speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS6","speckleOrd_c0066_spS8","speckleOrd_c0066_spS10"];
% files = ["speckleOrd_c0213_spS1","speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS6","speckleOrd_c0213_spS8","speckleOrd_c0213_spS10"];
% files = ["speckleBrow_c0007_spS1","speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS6","speckleBrow_c0007_spS8","speckleBrow_c0007_spS10"];

% choose long files
%files = ["245msSpeckleLar_spS0", "245msSpeckleLar_spS1", "245msSpeckleLar_spS3"]; 
%files = ["245msSpeckleMed_spS0", "245msSpeckleMed_spS1", "245msSpeckleMed_spS3"]; 
files = ["245msSpecklePar_spS0", "245msSpecklePar_spS1", "245msSpecklePar_spS3"]; 

% type = "OrdM";
% type = "OrdL";
type = "BrowP";

% set exposure time
imageSize = size(matfile(files(1)), 'speckle');
splitSetting = [5000,0]; % define frames for exposure time and dead time. Note: sum must be equal to numberOfFrames
splitPeriod = sum(splitSetting);
splitCount = idivide(int32(imageSize(3)), int32(splitPeriod));

% check if split equals frames
if mod(imageSize(3), splitPeriod) ~= 0
    error("ERROR: see split settings"); 
end

longSplitsMean = zeros(imageSize(1),imageSize(2), splitCount, length(files)); % long splits are equal to exposure time
shortSplitsMean = zeros(imageSize(1),imageSize(2), splitCount, length(files)); % short splits are equal to dead time

% generate images with exposure time
for i = 1:1:length(files)
    
    load(files(i));
    
    masterCursor = 1;
    for j = 1:splitCount
        % Cropping long segments
        masterCursorEnd = masterCursor + splitSetting(1) - 1;
        longSplitsMean(:,:,j,i) = mean(speckle(:,:,masterCursor:masterCursorEnd),3);
        masterCursor = masterCursorEnd + 1;
        
        % Cropping short segments
        masterCursorEnd = masterCursor + splitSetting(2) - 1;
        shortSplitsMean(:,:,j,i) = mean(speckle(:,:,masterCursor:masterCursorEnd),3);
        masterCursor = masterCursorEnd + 1;      
    end   
end

% Saves averaged images according to motion type
save(strcat('exposureData',type,num2str(splitSetting(1))),'longSplitsMean','shortSplitsMean','splitSetting');

% Visualize all averaged images for set exposure times and varying speckle sizes
% figure()
% for i=1:size(longSplitsMean,4)    
%     for j=1:size(longSplitsMean,3)
%         subplot(size(longSplitsMean,3), size(longSplitsMean,4), i + (j-1) * size(longSplitsMean,4))
%         imagesc(longSplitsMean(:,:,j,i));
%         axis('square')
%         colormap gray
%     end
% end

% Visualize 5 images with set exposure time of each speckle size (0.5, 1, 3)
figure()
for i=1:size(longSplitsMean,4)    
    for j=1:5
        subplot(5, size(longSplitsMean,4), i + (j-1) * size(longSplitsMean,4))
        imagesc(longSplitsMean(:,:,j,i));
        axis('square')
        colormap gray
    end
end

%% Comments: long- or shortSplitsMean = (X,Y,images,files) and SplitsMean(:,:,:,1) = 3-D matrix

files = "exposureDataOrdM5000";
%files = "exposureDataOrdL5000";
%files = "exposureDataBrowP5000";

load(files);

% Get temporal contrast
kernel = 9; % temporal kernel size
tLSCI = zeros(3,1);

for i = 1:size(longSplitsMean,4)
    tLSCI(i) = mean(getTLSCI(longSplitsMean(:,:,:,i),kernel,'cpu','none'),[1,2,3]);
end

sps = [0.5 1 3];

figure(1)
plot(sps,tLSCI,'-ko')
xlabel('Pixels per speckle')
ylabel('Temporal contrast (Kernel = 9)')
set(gca,'FontSize',12)

%% Estimate the optimal kernel size

% Choose files and fit with sortedTempData in section below
%files = "exposureDataOrdM5000";
%files = "exposureDataOrdL5000";
files = "exposureDataBrowP5000";

load(files);

kernelSize = 3:2:49;
tLSCI = zeros(size(longSplitsMean,4),length(kernelSize));

figure()
for i = 1:size(longSplitsMean,4)
    for j = 1:length(kernelSize)
        tLSCI(i,j) = mean(getTLSCI(longSplitsMean(:,:,:,i),kernelSize(j),'cpu','none'),[1,2,3]); % image size is changed to remove vignetting effect for contrast est.
    end

end
plot(kernelSize,tLSCI,'-o')
axis tight
grid on
legend('Speckle to pixel size ratio 0.5','Speckle to pixel size ratio 1','Speckle to pixel size ratio 3','Location','best')
ylabel('Temporal contrast')
xlabel('Kernel size')
set(gca,'Xtick',3:2:49,'FontSize',12)

%% Kernel analysis continued
% Note: fit type from section above with sortedTempData and run + save for
% all

% calculate kernel to speckle size ratio
spec = [0.1 1 3];
kernRatio2 = zeros(size(tLSCI));
for j=1:1:size(spec,2)
    kernRatio2(j,:) = (kernelSize)/spec(j);
end 
kern = kernRatio2'; kern = kern(:)';
con = tLSCI'; con = con(:)'; rcon = con./max(con);

% Choose type

% sortedTempDataOrdM = sortrows(([con ; kern]'),2);
% sortedTempDataOrdL = sortrows(([con ; kern]'),2);
 sortedTempDataBrowP = sortrows(([con ; kern]'),2);

save('sortedTempData.mat','sortedTempDataOrdM','sortedTempDataOrdL','sortedTempDataBrowP')

%% Result of kernel analysis

 load('sortedTempData.mat')

figure(3)
plot(sortedTempDataBrowP(:,2),sortedTempDataBrowP(:,1),'k.','LineWidth',0.5)
hold on
plot(sortedTempDataOrdM(:,2),sortedTempDataOrdM(:,1),'k*','LineWidth',0.5)
plot(sortedTempDataOrdL(:,2),sortedTempDataOrdL(:,1),'ko','LineWidth',0.5)
legend('Parenchyma','Medium vessel','Large vessel')
axis tight
%xlim([0 49]) % change to 49
ylim([0 0.4])
grid on
grid minor
%set(gca,'Xtick',0:2:49,'FontSize',16) % change to 49
ylabel('Temporal contrast')
xlabel('Kernel to speckle size ratio') %KernelWidth/SpeckleWidth
saveas(figure(3),strcat('TempContrastKernelRatio','.png'))



