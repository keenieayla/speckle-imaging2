%SpeckleSimulation - simulates speckle pattern and saves the pattern as a 
% .mat file for further processing. 
%
%% ------------- BEGIN CODE --------------

%-------- variable parameters -------- 

%Time
T = 15000; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 

%Volume 
sizeXYZ = 50;% mum 

%Particles
particlesN = 500;

%Sensor
% Set sensor to be 100x100 pixels
pixelsN = 25;
% Set pixelSize close to 1, 2, 4, 6, 8, 10
pixelSize = 1; % (mu m)

% motion parameters, use fractions here!
cBrownian = [1/100 1/150 1/200]; % constant factor for random motion
cOrdered = [1/5 1/10]; % constant factor for ordered motion

%% Simulate brownian motion
% tic
% for c = 1:1:length(cBrownian)
%     cDisp = cBrownian(c);
%     speckle = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cBrownian(c));  
%     save(strcat('speckleBrow',num2str(1/cBrownian(c))),'speckle','T','cDisp');
% end
% toc
%% Simulate ordered motion 
tic
for c = 1:1:length(cOrdered)
    cDisp = cOrdered(c);
    speckle = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrdered(c));
    save(strcat('speckleOrd',num2str(1/cOrdered(c))),'speckle','T','cDisp');
end
toc
%% ------------- END CODE --------------