%SpeckleSimulation for spatial analyzes 
%% Simulate all them damn speckles with proper sample sizes %% 
clear all
clc 

% Time
T = 5000; % no. of frames
dt = 1; % time steps of 1 mu second per frame

% Sensor
pixelsN = 100; % no. of pixels

% Load estimated pixel and speckle size
load('sp2pixSizeKAA','estPixSizeT','estSpSizeT')
estPixSize = estPixSizeT(1);
estSpSize = estSpSizeT(1);

% Optimal motion parameters (c value):    
load('tauFitbrow','qp');
cBrownian = qp;
load('tauFitord','qp');
cOrdered = qp; % constant for ordered motion in [medium large] vessel

% Volume 
sXY = 100; newSizeXY = ceil(estPixSize*sXY);

% Particles
pN = 1000; newParticlesN = ceil(pN.*(estPixSize.*estPixSize));
particleDensity = newParticlesN./(newSizeXY.*newSizeXY.*sXY); 


% Simulate brownian motion
tic
for pix = 1:1:length(estPixSize)
    cDisp = cBrownian;
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    sizeXY = newSizeXY(pix);
    particlesN = newParticlesN(pix);
    speckle = speckleSim('brownian',T,dt,sizeXY,particlesN,pixelsN,pixSize,cDisp);  
    %filename = strrep(sprintf('specklePar_spS%.0f',spSize),'.','');
    filename = strrep(sprintf('specklePar_spS05'),'.','');
    save(filename,'speckle','T','cDisp','spSize','pixSize','sizeXY','particlesN');
end
toc

% Simulate ordered motion Medium
tic
for pix = 1:1:length(estPixSize)
    cDisp = cOrdered(1);
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    sizeXY = newSizeXY(pix);
    particlesN = newParticlesN(pix);
    speckle = speckleSim('ordered',T,dt,sizeXY,particlesN,pixelsN,pixSize,cDisp);
    %filename = strrep(sprintf('speckleMed_spS%.0f',spSize),'.','');
    filename = strrep(sprintf('specklePar_spS05'),'.','');
    save(filename,'speckle','T','cDisp','spSize','pixSize','sizeXY','particlesN');
end
toc

% Simulate ordered motion Large
tic
for pix = 1:1:length(estPixSize)
    cDisp = cOrdered(2);
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    sizeXY = newSizeXY(pix);
    particlesN = newParticlesN(pix);
    speckle = speckleSim('ordered',T,dt,sizeXY,particlesN,pixelsN,pixSize,cDisp);
    %filename = strrep(sprintf('speckleLar_spS%.0f',spSize),'.','');
    filename = strrep(sprintf('specklePar_spS05'),'.','');
    save(filename,'speckle','T','cDisp','spSize','pixSize','sizeXY','particlesN');
end
toc
