% Global contrast 

%% ------------- BEGIN CODE --------------
clear all
% Calculate global contrast for different speckle-to-pixel size ratios
load('sp2pixSize','speckle');
speckleEst=speckle;

load('sp2pixSizeKAA','estPixSizeT','estSpSizeT','reqSpSizeT');
estPixSize=estPixSizeT;
estSpSize=estSpSizeT;
reqSpSize=reqSpSizeT;


%type='Ord_c0066';name='Medium';
%type='Ord_c0213';name='Large';
type='Brow_c0007';name='Parenchyma';

%%
expTime=5; %mus 
avImages=zeros(length(speckleEst),length(speckleEst),length(estSpSize));
%gCon = zeros(length(estSpSize),1);
for idx = 1:1:length(estSpSize)
    spSize=reqSpSize(idx);
        if spSize==0.5; 
            fileToLoad=(sprintf('speckle%s_spS05',type)); %=(sprintf('speckleLar_spS%.0f',spSize));  
        else 
            fileToLoad=(sprintf('speckle%s_spS%.0f',type,spSize)); %=(sprintf('speckleLar_spS%.0f',spSize));
        end 
    load(fileToLoad); 
    
    avImage5ms = mean(speckle(:,:,1:(expTime*1000)),3);
    avImages(:,:,idx) = avImage5ms(:,:);
    save(fileToLoad,'avImage5ms','expTime','-append');
    
%     globalContrast=std(avImage5ms(:))./mean(avImage5ms(:));
%     gCon(idx)=globalContrast;
%     save(fileToLoad,'globalContrast','-append');
end     


%% View 5ms averaged images 

figure(1)
for idx = 1:1:length(estSpSize)
    subplot(2,4,idx)
    imagesc(avImages(:,:,idx)) 
    title(sprintf('Speckle Size: %.2f',reqSpSize(idx)))
    colormap gray
    axis image
end     
saveas(figure(1),strcat(sprintf('5ms%sImage',name),'.png'));


%% calculate global contrast 
%type='Ord_c0066';name='Medium';
%type='Ord_c0213';name='Large';
type='Brow_c0007';name='Parenchyma';

gCon = zeros(length(estSpSize),1);
for idx = 4%1:1:length(estSpSize)
    spSize=reqSpSize(idx);
        if spSize==0.5; 
            fileToLoad=(sprintf('speckle%s_spS05',type)); %=(sprintf('speckleLar_spS%.0f',spSize));  
        else 
            fileToLoad=(sprintf('speckle%s_spS%.0f',type,spSize)); %=(sprintf('speckleLar_spS%.0f',spSize));
        end 
    load(fileToLoad); 
    
    globalContrast=std(avImage5ms(:))./mean(avImage5ms(:));
    gCon(idx)=globalContrast;
    save(fileToLoad,'globalContrast','-append');
end  




%%
figure(4)
plot(reqSpSize,gCon)

