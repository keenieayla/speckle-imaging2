%% Clean up
close all
clear all

%% Load Speckle Image
% select 'speckles_brownian', 'speckles_orderedL', or 'speckles_orderedM'.

%Motion parameters
tauCorderedL = 20; %frames
tauCorderedM = 80; %frames
tauCBrownianP = 500; %frames

cOrderedL = 1/tauCorderedL; % constant factor for ordered motion, %% large vessel: ordered motion, tau_c = 20 frames
cOrderedM = 1/tauCorderedM; % constant factor for ordered motion, %% mid-sized vessel: ordered motionl tau_c = 80 frames
cBrownianP = 1/tauCBrownianP; % constant factor for random motion, %% parenchyma: unordered motion tau_c = 500 mu s


load('speckleBrowP')
load('speckleOrdM')
load('speckleOrdL')

speckles_brownian = speckleBrowP(:,:,:,1);
speckles_orderedL = speckleOrdL(:,:,:,1);
speckles_orderedM = speckleOrdM(:,:,:,1);

T = size(speckleBrowP,3);

% I_t = speckles_brownian;
% I_t = speckles_orderedL;
% I_t = speckles_orderedM;

%% Viusualize Speckle Pattern

% figure(1)
% title('Speckle Pattern')
% axis image
% for t = 1:1:T
%     imagesc(speckles_brownian(:,:,t))   
%     pause(.1)
% end

%% Temporal Autocovariance

maxLag = 20;

% The average autocorrelation: 
g2oM = getG2_Dmitry(speckles_orderedM,maxLag);
g2oL = getG2_Dmitry(speckles_orderedL,maxLag);
g2b = getG2_Dmitry(speckles_brownian,maxLag);


%% Plot Temporal Autocovariance
figure(4)
semilogx(g2oM(:),'DisplayName',['Medium size vessel (ordered, ' ,sprintf('c: %.2f)',cOrderedM)])
hold on
semilogx(g2oL(:),'DisplayName',['Large size vessel (ordered, ' ,sprintf('c: %.2f)',cOrderedL)])
semilogx(g2b(:),'DisplayName',['Parenchyma (brownian, ' ,sprintf('c: %.4f)',cBrownianP)])
legend 
xlabel('Time lag \tau, \mus')
ylabel('g2(\tau)')
title('Temporal Autocorrelation (g2)')
%saveas(gcf,'run1','fig')

% Plot
figure(2)
plot(g2oM(:),'DisplayName','Medium size vessel, Ordered motion')
hold on
plot(g2oL(:),'DisplayName','Large size vessel, Ordered motion')
plot(g2b(:),'DisplayName','Parenchyma, Brownian motion')
legend
axis tight

