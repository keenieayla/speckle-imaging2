% Estimate pixel sizes, to get speckle to pixel ratios 
%% ------------- BEGIN CODE --------------
clear all
%-------- simulation parameters -------- 

%%Time
T = 1; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 

%%Volume 
sizeXYZ = 100;% mum 

%%Particles
particlesN = 1000;

%%Sensor
pixelsN = 100; % 100x100 pixels, in theory we would like even larger

%%Optimal c values:    
%cBrownianP = 0.002;
%cOrderedM = 0.064;
cOrderedL = 0.2295;

%%Fabis optimal c values
% cBrownianP = 0.0074
% cOrderedM = 0.066
% cOrderedL = 0.213

%% Set different pixel sizes and evaluate the speckle to pixel size ratio. 

pixelSize = [1 2 4 6 8 10]; % (mu m)

%% Simulate speckle patteren for different pixel sizes 

tic
for i = 1:1:length(pixelSize)
    pix = pixelSize(i);
    speckleOrdL = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(i),cOrderedL);  
    if pix >= 1
        save(strcat('pixelSize',num2str(pix)),'speckleOrdL','T','pix');  
    else 
        save('pixelSize05','speckleOrdL','T','pix');    
    end 
end
toc

%% Visualize the speckle patterns 

figure('Name','Speckle patterns')
for i = 1:1:length(pixelSize)
    pix = pixelSize(i);
    if pix >= 1
        load(strcat('pixelSize',num2str(pix)))
    else 
        load('pixelSize05')
    end 
    subplot(3,3,i)
    axis image
    imagesc(speckleOrdL(:,:)) 
    title(strcat('pixelSize',num2str(pix)))
end 

%% Speckle size estimation

maxLags = 20; % the max expected width of a speckle, see SpecklePatternVisualization for 0.5

sSize = [];
for i = 1:1:length(pixelSize)
    pix = pixelSize(i);
    if pix >= 1
        load(strcat('pixelSize',num2str(pix)))
        [spSize,corr] = getSpeckleSize(speckleOrdL,maxLags); % compare to dmitry's corr
        save(strcat('pixelSize',num2str(pix)),'spSize','corr','-append');
        sSize(i) = spSize ;
    else 
        load('pixelSize05')
        [spSize,corr] = getSpeckleSize(speckleOrdL,maxLags); % compare to dmitry's corr
        save('pixelSize05','spSize','corr','-append');  
        sSize(i) = spSize ;
    end 
end 

%% Fitting the curve, to get the pixel sizes for ratios of [1 2 4 6 8 10]

[xData, yData] = prepareCurveData(pixelSize, sSize);

ft = fittype( 'power1' );
opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.StartPoint = [6.92047132512014 -0.952803010419301];

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );
% Goodness of fit: R-square: 0.9981
% General model Power1: f(x) = a*x^b,  a = 6.76, b = -0.9525

% Plot fit with data.

figure()
h = plot(fitresult,'--k', xData, yData,'ok');
legend( h, 'Speckle Size vs. Pixel Size', 'Power Fit', 'Location', 'NorthEast', 'Interpreter', 'none' );
xlabel( 'Pixel Size (pixels)', 'Interpreter', 'none' );
ylabel( 'Speckle Size (pixels)', 'Interpreter', 'none' );
axis tight
grid on


%% Solving and calculating pixel size for ratios of [1 2 4 6 8 10]
syms pixS ratio

a=6.76; b=-0.9525;
eqn = solve(ratio == a*(pixS^b),pixS)

Ratios = [1 2 4 6 8 10]; 
pixelSizes = [];
for i = 1:1:length(Ratios)
    ratio = Ratios(i);
    pixelSizes(i) = 1/((25*ratio)/169)^(400/381); % solved from above solve
end 


%% Plot it 

% Plot
figure(1)
plot(pixelSize,sSize,'--k')
hold on
for i = 1:1:length(Ratios)
    plot(pixelSizes(i),Ratios(i),'ok')
end 
ylabel('Speckle to Pixel Size Ratio')
xlabel('Pixel Size')
grid on

% This plots shows that at pixel sizes of 7-10+ mum, the pixel size ratio 
% is close to 1. However moving below 6mum pixel size, the ratio starts to 
% increase. Thus we explore values below this threshold, to quantify the
% speckle-to-pixel size ratios. From this we select a number of ratios and 
% their corresponding pixel sizes, for future simulations.


%% Visualize the new speckle patterns 
tic
newSpeckles = [];
for i = 1:1:length(pixelSizes)
    pix = pixelSizes(i);
    newSpeckles(:,:,i) = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSizes(i),cOrderedL);  
end
toc

figure('Name','New Speckle Patterns')
for i = 1:1:length(pixelSizes)
    pix = pixelSizes(i);
    ratio = Ratios(i);
    subplot(2,3,i)
    axis image
    imagesc(newSpeckles(:,:,i)) 
    title(strcat('Speckle to Pixel Size Ratio:',num2str(ratio)))
end 


% Pixelsizes should be (in mum):
% 7.4359    3.5916    1.7348    1.1334    0.8379    0.6629
% to obtain the desired speckle-to-pixel ratios. 



%% ------------- END CODE --------------