%% Clean up
close all
clear all
clc

%% Load Speckle Image
% select 'speckles_brownian', 'speckles_orderedL', or 'speckles_orderedM'.

% load('speckles_brownian')
% load('speckles_orderedL')
% load('speckles_orderedM')

%% Viusualize Speckle Pattern

figure(1)
title('Speckle Pattern')
axis image
for t = 1:1:50
    imagesc(speckleOrdL(:,:,t))   
    pause(.1)
end

%% Temporal Autocorrelation

maxLag = 640;

% The average temporal autocorrelation: 
% g2B = getG2(speckleBrowP,maxLag);
g2oM = getG2(speckleOrdM,maxLag);
g2oL = getG2(speckleOrdL,maxLag);

%% Model

%YbrowP=g2B(1:find(g2B<1,1,'first'));
YordM=g2oM(1:find(g2oM<1,1,'first'));
YordL=g2oL(1:find(g2oL<1,1,'first'));

%tBrowP=(0:length(YbrowP)-1)';
tOrdM=(0:length(YordM)-1)';
tOrdL=(0:length(YordL)-1)';

% Set up fittype and options.
ftOrdered = fittype( '1+beta*(exp(-(x/tauc)^2))^2 +C', 'independent', 'x', 'dependent', 'y' );
%ftBrownian = fittype( '1+beta*(exp(-(x/tauc)^.5))^2 +C', 'independent', 'x', 'dependent', 'y' );

opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Lower = [0 0 1];
opts.Robust = 'LAR';
% opts.StartPoint = [0.603588936308743 0.894907993317702 0.774151184307465];

% Fit model to data.
%[fitBrowP, ~] = fit( tBrowP, YbrowP, ftBrownian, opts );
[fitOrdM, ~] = fit( tOrdM, YordM, ftOrdered, opts );
[fitOrdL, ~] = fit( tOrdL, YordL, ftOrdered, opts ); % 2nd output fitting param

% Get model parameter
%tauCbrowP = fitBrowP.tauc;
tauCordM = fitOrdM.tauc;
tauCordL = fitOrdL.tauc;

% Save to original .mat file
%save(strcat('speckleBrowP',num2str(1/cBrownianP)),'tauCbrowP','g2B','-append');
save(strcat('speckleOrdM','0_064'),'tauCordM','g2oM','-append');
save(strcat('speckleOrdL','0_2295'),'tauCordL','g2oL','-append');

%% Plot

% Plot fit with data.
figure;
% subplot(1,3,1)
% plot( fitBrowP, tBrowP, YbrowP )
% grid on
% title(sprintf('Parenchyma (brownian, c: %.4f)',cBrownianP));
subplot(1,3,2)
plot( fitOrdM, tOrdM, YordM )
grid on
title(sprintf('Medium size vessel (ordered, c: %.2f)',cOrderedM));
subplot(1,3,3)
plot( fitOrdL, tOrdL, YordL )
title(sprintf('Large size vessel (ordered, c: %.2f)',cOrderedL));
grid on

%% Load data
%%% ------------ START HERE -------------- %%% 

% % Medium vessel data for T = 5000
% load('speckleOrdM30')
% load('speckleOrdM80')
% load('speckleOrdM120')
% 
% % Large vessel data for T = 5000
% load('speckleOrdL5')
% load('speckleOrdL20')
% load('speckleOrdL50')

% Optimal c values for L and M for T = 5000, g_2 w. maxLag = 640
load('speckleOrdL0_2295') % for c = 0.2295
load('speckleOrdM0_064') % for c = 0.064

%% Get c = X and 1/Tau = Y

[XordM,YordM] = getTau('speckleOrdM30','speckleOrdM80','speckleOrdM120','ordM');
[XordL,YordL] = getTau('speckleOrdL5','speckleOrdL20','speckleOrdL50','ordL');

%% Plot 1/Tau and c for medium vessel

fitOrdM = polyfit(XordM,YordM,1); 
xOrdM = linspace(0,0.4,801);
yOrdM = polyval(fitOrdM,xOrdM);

%fprintf('for tauC = 80. 1/tauC = 0.0125')
% find(yOrdM==0.0125)

% Plot tau and c for medium vessel
figure()
plot(XordM,YordM,'o')
hold on
plot(xOrdM,yOrdM)
hold on
scatter(xOrdM(129),yOrdM(129),'x','k')
snapnow;
hold off
legend('Tau_c values from ordM model','Linear fit','Tau_c at 80')
ylabel('1/Tau_c')
xlabel('c')
grid on
axis tight
set(gca,'FontSize',12)

%% Plot 1/Tau and c for large vessel

fitOrdL = polyfit(XordL,YordL,1); 
xOrdL = linspace(0,0.4,801);
yOrdL = polyval(fitOrdL,xOrdL);

%fprintf('for tau_c = 20. 1/tauC = 0.05')
% find(yOrdL==0.05) %

% Plot tau and c for medium vessel
figure()
plot(XordL,YordL,'o')
hold on
plot(xOrdL,yOrdL)
hold on
scatter(xOrdL(460),yOrdL(460),'x','k')
snapnow;
hold off
legend('Tau_c values from ordL model','Linear fit','1/Tau_c at 20')
ylabel('1/Tau_c')
xlabel('c')
grid on
axis tight
set(gca,'FontSize',12)

%% Plot Temporal Autocovariance

% Plot
figure()
plot(g2oM(:),'DisplayName','Medium vessel, Ordered motion')
hold on
plot(g2oL(:),'DisplayName','Large vessel, Ordered motion')
%plot(g2b(:),'DisplayName','Parenchyma, Brownian motion') %% add brownian
legend
ylabel('g_2(\tau)')
xlabel('Time lag, \tau (\mu s)')
title('Temporal Autocorrelation')
axis tight

figure()
semilogx(g2oM(:),'DisplayName',['Medium size vessel (ordered, ',sprintf('c: %.3f)',cOrderedM)])
hold on
semilogx(g2oL(:),'DisplayName',['Large size vessel (ordered, ',sprintf('c: %.4f)',cOrderedL)])
%semilogx(g2b(:),'DisplayName',['Parenchyma (brownian, ' ,sprintf('c: %.4f)',cBrownianP)])
legend 
xlabel('Time lag \tau, \mus')
ylabel('g2(\tau)')
title('Temporal Autocorrelation (semilogx)')
axis tight
saveas(gcf,'run1','fig')