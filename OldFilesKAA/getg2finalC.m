%% Clean up
close all
clear all
clc

%SpeckleSimulation - simulates speckle pattern and saves the pattern as a 
% .mat file for further processing. 
%
%% ------------- BEGIN CODE --------------

%-------- variable parameters -------- 

%Time
T = 150000; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 

%Volume 
sizeXYZ = 50;% mum 

%Particles
particlesN = 500;

%Sensor
% Set sensor to be 100x100 pixels
pixelsN = 25;
% Set pixelSize close to 1, 2, 4, 6, 8, 10
pixelSize = 1; % (mu m)

% motion parameters, use fractions here!
cBrownian = 0.0074; % constant factor for random motion
cOrdered = [0.066 0.213]; % constant factor for ordered motion M and L

%% Simulate brownian motion
tic
for c = 1:1:length(cBrownian)
    cDisp = cBrownian(c);
    speckle = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cBrownian(c));  
    save(strcat('speckleBrow',num2str(1/cBrownian(c))),'speckle','T','cDisp');
    
    filename = strcat('speckleBrow',num2str(round(cDisp,4)));
    filename = strrep(filename,'.','');
    save(filename,'speckle','T','cDisp');
end
toc
%% Simulate ordered motion 
% tic
% for c = 1:1:length(cOrdered)
%     cDisp = cOrdered(c);
%     speckle = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrdered(c));
%     
%     filename = strcat('speckleOrd',num2str(round(cDisp,4)));
%     filename = strrep(filename,'.','');
%     save(filename,'speckle','T','cDisp');
% end
% toc
%% Select files for g2 analysis
% caclulate the g2 function and saves it as a .mat-file

% choose files from which to calc g2
files = ["speckleBrow00074"];
%files = ["speckleOrd0066","speckleOrd0213"];

% choose motion type for each file
type = ["brow"]; % either "brow" or "ord"
% type = ["ord","ord"];

for i = 1:1:length(files)
    
    load(files(i));
    
    % Temporal Autocorrelation
    maxLag = 15;

    % The average temporal autocorrelation: 
    g2 = getG2(speckle,maxLag);
    
    % Save g2 function, together with T and specific cDisp
    filename = strcat('g2',type(i),num2str(round(cDisp,4)));
    filename = strrep(filename,'.','');
    save(filename,'g2','T','cDisp');

end

%% Plot g2 (Temporal Autocorrelation)

% plot g2 function on a semilog axis
figure(1)
% choose files to plot
files = ["g2ord0066","g2ord0213","g2brow00074"]; % all motions
    
for i = 1:1:length(files)
    load(files(i));    
    semilogx(g2), hold on
end
xlabel('Time lag (\tau)')
ylabel('g_2(\tau)')
legend('Ordered motion (c: 0.066)','Ordered motion (c: 0.213)','Brownian motion (c: 0.0074)')
saveas(figure(1),'g2all.png')

%% ------------- END CODE --------------
