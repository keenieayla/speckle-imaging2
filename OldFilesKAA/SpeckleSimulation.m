%SpeckleSimulation - simulates speckle pattern and saves the pattern as a 
% .mat file for further processing. 
%
%% ------------- BEGIN CODE --------------

%-------- variable parameters -------- 

%Time
T = 5000; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 

%Volume 
sizeXYZ = 100;% mum 

%Particles
particlesN = 1000;

%Sensor
% Set sensor to be 100x100 pixels
pixelsN = 100;
% Set pixelSize close to 1, 2, 4, 6, 8, 10
pixelSize = 1; % (mu m)

% motion parameters, use fractions here!
% cBrownianP = [1/500 1/300 1/800]; % constant factor for random motion, %% parenchyma: unordered motion tau_c = 500 mu s
% cOrderedM = [1/80 1/30 1/120]; % constant factor for ordered motion, %% mid-sized vessel: ordered motionl tau_c = 80 frames
% cOrderedL = [1/20 1/5 1/50]; % constant factor for ordered motion, %% large vessel: ordered motion, tau_c = 20 frames

% Optimal c values
cOrderedM = 0.064;
cOrderedL = 0.2295;

%% Simulate brownian motion

tic
% for c = 1:1:length(cBrownianP)
%     cBrowP = cBrownianP(c);
% %     speckleBrowP = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cBrownianP(c));  
%     save(strcat('speckleBrowP',num2str(1/cBrownianP(c))),'speckleBrowP','T','cBrowP');
% end

%% Simulate ordered motion medium vessel
for c = 1:1:length(cOrderedM)
    cOrdM = cOrderedM(c);
    speckleOrdM = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrderedM(c));
%   save(strcat('speckleOrdM',num2str(1/cOrderedM(c))),'speckleOrdM','T','cOrdM');
    save(strcat('speckleOrdM','0_064'),'speckleOrdM','T','cOrdM');
end

%% Simulate ordered motion large vessel
for c = 1:1:length(cOrderedL)
    cOrdL = cOrderedL(c);
    speckleOrdL = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrderedL(c));  
%   save(strcat('speckleOrdL',num2str(1/cOrderedL(c))),'speckleOrdL','T','cOrdL');
    save(strcat('speckleOrdL','0_2295'),'speckleOrdL','T','cOrdL');
end
toc

%% TEMP FIX WITH C VALUES %%
% Simulate ordered motion medium vessel
% for c = 1:1:length(cBrownianP)
%     cBrowP = cBrownianP(c);
% %     speckleBrowP = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cBrownianP(c));  
%     save(strcat('speckleBrowP',num2str(1/cBrownianP(c))),'cBrowP','-append');
% end
% Simulate ordered motion medium vessel
% for c = 1:1:length(cOrderedM)
%     cOrdM = cOrderedM(c);
%     speckleOrdM = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrderedM(c));
%   % save(strcat('speckleOrdM',num2str(1/cOrderedM(c))),'cOrdM','-append');
%     save(strcat('speckleOrdM',num2str(1/cOrderedM(c))),'cOrdM','-append');
% end
% 
% % Simulate ordered motion large vessel
% for c = 1:1:length(cOrderedL)
%     cOrdL = cOrderedL(c);
%     speckleOrdL = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize,cOrderedL(c));  
%   % save(strcat('speckleOrdL',num2str(1/cOrderedL(c))),'cOrdL','-append'); 
%     save(strcat('speckleOrdM',num2str(1/cOrderedM(c))),'cOrdM','-append');
% end

%% ------------- END CODE --------------