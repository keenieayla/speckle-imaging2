%SpeckleSimulation
%% Simulate all them damn speckles with proper sample sizes %% 
clear all
clc 

% Time
T = 5000*49; % no. of frames
dt = 1; % time steps of 1 mu second per frame

% Sensor
pixelsN = 10; % no. of pixels

% Load estimated pixel and speckle size
load('sp2pixSizeKAA','estPixSizeT','estSpSizeT','reqSpSizeT')
estPixSize=[estPixSizeT(1) estPixSizeT(2) estPixSizeT(4)];
estSpSize=[estSpSizeT(1) estSpSizeT(2) estSpSizeT(4)];
reqSpSize=[reqSpSizeT(1) reqSpSizeT(2) reqSpSizeT(4)];

% Optimal motion parameters (c value):    
load('tauFitbrow','qp');
cBrownian = qp;
load('tauFitord','qp');
cOrdered = qp; % constant for ordered motion in [medium large] vessel

% Volume 
sXY = 10; newSizeXY = ceil(estPixSize*sXY);

% Particles
pN = 10; newParticlesN = ceil(pN.*(estPixSize.*estPixSize));
particleDensity = newParticlesN./(newSizeXY.*newSizeXY.*sXY); 

%% Simulate brownian motion
tic
for pix = 1:1:length(estPixSize)
    cDisp = cBrownian;
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    sizeXY = newSizeXY(pix);
    particlesN = newParticlesN(pix);
    speckle = speckleSim('brownian',T,dt,sizeXY,particlesN,pixelsN,pixSize,cDisp);  
    filename = strrep(sprintf('245msSpecklePar_spS%.0f',spSize),'.','');
    save(filename,'speckle','T','cDisp','spSize','pixSize','sizeXY','particlesN');
    toc
end
toc

% Simulate ordered motion Medium
tic
for pix = 1:1:length(estPixSize)
    cDisp = cOrdered(1);
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    sizeXY = newSizeXY(pix);
    particlesN = newParticlesN(pix);
    speckle = speckleSim('ordered',T,dt,sizeXY,particlesN,pixelsN,pixSize,cDisp);
    filename = strrep(sprintf('245msSpeckleMed_spS%.0f',spSize),'.','');
    save(filename,'speckle','T','cDisp','spSize','pixSize','sizeXY','particlesN');
    toc
end
toc

% Simulate ordered motion Large
tic
for pix = 1:1:length(estPixSize)
    cDisp = cOrdered(2);
    spSize = estSpSize(pix);
    pixSize = estPixSize(pix);
    sizeXY = newSizeXY(pix);
    particlesN = newParticlesN(pix);
    speckle = speckleSim('ordered',T,dt,sizeXY,particlesN,pixelsN,pixSize,cDisp);
    filename = strrep(sprintf('245msSpeckleLar_spS%.0f',spSize),'.','');
    save(filename,'speckle','T','cDisp','spSize','pixSize','sizeXY','particlesN');
    toc
end
toc
