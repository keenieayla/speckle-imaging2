%SpeckleSimulation - simulates speckle pattern and saves the pattern as a 
% .mat file for further processing. 
%
%% ------------- BEGIN CODE --------------

%-------- variable parameters -------- 

%Time
T = 10; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 

%Volume 
sizeXYZ = 100;% mum 

%Particles
particlesN = 1000;

%Sensor
% Set sensor to be 100x100 pixels
pixelsN = 100;
% Set pixelSize close to 1, 2, 4, 6, 8, 10
pixelSize = [1 10]; %4 6 8 10]; % (mu m)


%Motion parameters
tauCorderedL = 20; %frames
tauCorderedM = 80; %frames
tauCBrownianP = 500; %frames

cOrderedL = 1/tauCorderedL; % constant factor for ordered motion, %% large vessel: ordered motion, tau_c = 20 frames
cOrderedM = 1/tauCorderedM; % constant factor for ordered motion, %% mid-sized vessel: ordered motionl tau_c = 80 frames
cBrownianP = 1/tauCBrownianP; % constant factor for random motion, %% parenchyma: unordered motion tau_c = 500 mu s

%-------- prepare output --------

speckleBrowP = NaN(pixelsN,pixelsN,T,length(pixelSize));
speckleOrdM = NaN(pixelsN,pixelsN,T,length(pixelSize));
speckleOrdL = NaN(pixelsN,pixelsN,T,length(pixelSize));

speckleBrowP(:,:,:) = speckleSim('brownian',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(pix),cBrownianP);
waitbar(rep/totRep,f);

speckleOrdL(:,:,:,pix) = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(pix),cOrderedL);
waitbar(rep/totRep,f);

speckleOrdM(:,:,:,pix) = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(pix),cOrderedM);
waitbar(rep/totRep,f);

save('speckleBrowP','speckleBrowP');
save('speckleOrdL','speckleOrdL');
save('speckleOrdM','speckleOrdM');





%% ------------- END CODE --------------