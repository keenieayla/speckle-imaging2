% Estimate pixel sizes, to get speckle to pixel ratios 

%% ------------- BEGIN CODE --------------
clear all
close all
%-------- simulation parameters -------- 
% Time
T = 1; % T being the number of frames (5000 = 5 ms recording)
dt = 1; % with dt being time steps of 1 mu second per frame , 500 mus 
% Volume 
sizeXYZ = 100;% mum 
% Particles
particlesN = 1000;
% Sensor
pixelsN = 100; % 100x100 pixels, in theory we would like even larger

cDisp = 0;

% Set different pixel sizes and evaluate the speckle to pixel size ratio. 
pixelSize = [0.5:0.02:1.5,2:1:7]; % more needed in lower pixel sizes!

%% Simulate speckle patteren for different pixel sizes 

for idx = 1:1:length(pixelSize)
    pix = pixelSize(idx);
    speckle(:,:,idx) = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,pixelSize(idx),cDisp);  
end

%% Speckle size estimation
maxLags = 20; % the max expected width of a speckle, see SpecklePatternVisualization for 0.5

% estimate speckle size for different pixel sizes 
sSize = NaN(length(pixelSize),1);
for idx = 1:1:length(pixelSize)
    pix = pixelSize(idx);
    [sSize(idx),corr] = getSpeckleSize(speckle(:,:,idx),maxLags); % compare to dmitry's corr
end 

%% Fitting the curve, to get the pixel sizes for ratios of [1 2 4 6 8 10]
[xData, yData] = prepareCurveData( pixelSize, sSize);
ft = fittype( 'linearinterp' );

% Fit model to data.
[fitSp2Pix, ~] = fit( xData, yData, ft);

x = [0.5:0.001:10]';
y = fitSp2Pix(x);
reqSpSize = [1 2 4 6 8 10];
estPixSize = NaN(length(reqSpSize),1);
for idx = 1:length(reqSpSize)
    estPixSize(idx) = x(find(y<=reqSpSize(idx),1));
end

%% Plot fit with data.
figure(1) % speckle to pixel size
plot(x,y,'--k', estPixSize,reqSpSize,'*k')
for idx = 1:length(estPixSize) 
    text(estPixSize(idx),reqSpSize(idx)+0.3,sprintf("  [%.2f,%.1f]    ",...
        estPixSize(idx),reqSpSize(idx)),'Position',[1 0],'HorizontalAlignment','left')
end
xlim([0,10])
legend( 'Speckle size vs. Pixel size', 'Estimated Ratios', 'Location', 'NorthEast', 'Interpreter', 'none' );
xlabel( 'Pixel size [mum]', 'Interpreter', 'none' );
ylabel( 'Speckle size [pixels]', 'Interpreter', 'none' );
grid on

%% Check if it is correct

% generate speckle for estimated pixle size
for idx = 1:1:length(estPixSize)
    speckleEst(:,:,idx) = speckleSim('ordered',T,dt,sizeXYZ,particlesN,pixelsN,estPixSize(idx),cDisp);  
end

maxLags = 20; % the max expected width of a speckle, see SpecklePatternVisualization for 0.5

% estimate speckle size for estimated pixle size
estSpSize = NaN(length(estPixSize),1);
for idx = 1:1:length(estPixSize)
    [estSpSize(idx),~] = getSpeckleSize(speckleEst(:,:,idx),maxLags); % compare to dmitry's corr
end

% calculate the error
err = sqrt((estSpSize - reqSpSize').^2);

figure(2) % view speckles with required speckle size
for i = 1:1:length(estSpSize)
    subplot(2,3,i)
    imagesc(speckleEst(:,:,i)) 
    title(sprintf('Speckle Size: %.2f',estSpSize(i)))
    axis image
end

%% Save, this is all we need to save!

filename = 'sp2pixSize';
save(filename,'speckle','reqSpSize','estSpSize','estPixSize','fitSp2Pix','err');
saveas(figure(1),strcat(filename,'.png'));
saveas(figure(2),strcat(filename,'Speckles','.png'));

%% For reference, these are the old results!!
% Pixelsizes should be (in mum):
% 7.4359    3.5916    1.7348    1.1334    0.8379    0.6629
% to obtain the desired speckle-to-pixel ratios. 


%% ------------- END CODE --------------