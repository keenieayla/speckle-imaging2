
clear all; close all; clc;

%% -------- Create image of 5 ms -------- %%

% Choose 1 file

%files = ["speckleOrd_c0066_spS1","speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS6","speckleOrd_c0066_spS8","speckleOrd_c0066_spS10"];
%files = ["speckleOrd_c0213_spS1","speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS6","speckleOrd_c0213_spS8","speckleOrd_c0213_spS10"];
files = ["speckleBrow_c0007_spS1","speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS6","speckleBrow_c0007_spS8","speckleBrow_c0007_spS10"];

% Choose motion type for file
%type = "OrdM";
%type = "OrdL";
type = "BrowP";

for i = 1:1:length(files)
    load(files(i));
    mImage(:,:,i) = mean(speckle(:,:,1:5000),3);
    save(strcat('mData',type),'mImage');
end

%% Kernel Size Analysis 

% Choose files

% files = "mDataOrdM";
% files = "mDataOrdL";
 files = "mDataBrowP";

%figure(1)
kernelSize = 1:2:49;
contrast = zeros(6,length(kernelSize));
for i = 1:1:length(files)
    load(files(i));
    for idx = 1:1:length(kernelSize)
    contrast(:,idx) = mean(getSLSCI(mImage(25:75,25:75,:),kernelSize(idx),'cpu','none'),[1,2]); % calculates contrast from center pixels
    end
end
% plot(kernelSize,contrast)
% axis tight
% grid on
% legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
% ylabel('Contrast')
% xlabel('Kernel size')
% % saves figure and a .png file in directory according to motion type
% saveas(figure(1),strcat('Contrast_vs_Kernel_size_',type,'.png'))

% calculate kernel to speckle size ratio
spec = [1 2 4 6 8 10];
kernRatio2 = zeros(size(contrast));
for j=1:1:size(spec,2)
    kernRatio2(j,:) = (kernelSize)/spec(j);
end 
kern = kernRatio2'; kern = kern(:)';
con = contrast'; con = con(:)'; rcon = con./max(con);

% Choose type

% sortedDataOrdM = sortrows(([con ; kern]'),2);
% sortedDataOrdL = sortrows(([con ; kern]'),2);
 sortedDataBrowP = sortrows(([con ; kern]'),2);

% save('sortedData.mat','sortedDataOrdM','sortedDataOrdL','sortedDataBrowP')

%%
load('sortedData.mat')

figure(2)
plot(sortedDataBrowP(:,2),sortedDataBrowP(:,1),'k.','LineWidth',0.5)
hold on
plot(sortedDataOrdM(:,2),sortedDataOrdM(:,1),'k*','LineWidth',0.5)
plot(sortedDataOrdL(:,2),sortedDataOrdL(:,1),'ko','LineWidth',0.5)
legend('Parenchyma','Medium vessel','Large vessel')
axis tight
xlim([0 20]) % change to 49
ylim([0 0.4])
grid on
%grid minor
set(gca,'Xtick',0:2:20,'FontSize',16) % change to 49
ylabel('Spatial contrast')
xlabel('Kernel to speckle size ratio') %KernelWidth/SpeckleWidth
saveas(figure(2),strcat('ContrastKernelRatio','.png'))





