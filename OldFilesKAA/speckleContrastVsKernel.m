
clear all; close all; clc;

%% Add files to Matlab path

% Add data to path
addpath('Data/Speckles') % add path to keenies new data files from dropbox
% Add functions to path
addpath('Functions') 

%% -------- Create image of 5 ms -------- %%

% Choose 1 file

% files = ["speckleOrd_c0066_spS1","speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS6","speckleOrd_c0066_spS8","speckleOrd_c0066_spS10"];
 files = ["speckleOrd_c0213_spS1","speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS6","speckleOrd_c0213_spS8","speckleOrd_c0213_spS10"];
% files = ["speckleBrow_c0007_spS1","speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS6","speckleBrow_c0007_spS8","speckleBrow_c0007_spS10"];

% Choose motion type for file

% type = "OrdM";
 type = "OrdL";
% type = "BrowP";

for i = 1:1:length(files)
    
    load(files(i));
    mImage(:,:,i) = mean(speckle(:,:,1:5000),3);
    % Saves averaged images according to motion type
    save(strcat('mData',type),'mImage');
end

%% -------- Calculate Speckle Contrast vs. Kernel Size -------- %%

% Choose 1 file to plot

 files = "mDataOrdM";
% files = "mDataOrdL";
% files = "mDataBrowP";

% Visualize averaged images
% figure(1)
% for i = 1:6 % length of files
%     subplot(1,6,i)
%     imagesc(mImage(:,:,i))
%     axis('square')
%     colormap gray
% end

% Plot
figure(2)
kernelSize = 1:2:49;
contrast = zeros(6,length(kernelSize));

for i = 1:1:length(files)
    load(files(i));
    
    for idx = 1:1:length(kernelSize)
        contrast(:,idx) = mean(getSLSCI(mImage(:,:,:),kernelSize(idx),'cpu','none'),[1,2]); % image size is changed to remove vignetting effect for contrast est.
    end, hold on

end

plot(kernelSize,contrast)
axis tight
grid on
legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
ylabel('Contrast')
xlabel('Kernel size')
% saves figure and a .png file in directory according to motion type
% saveas(figure(2),strcat('Contrast_vs_Kernel_size_',type,'.png'))

%% Find optimal trade off point

nPoints = length(kernelSize);
for i = 1:size(contrast,1)
    allCoord = [kernelSize;contrast(i,:)]';

    % Pull out first point
    firstPoint = allCoord(1,:);

    % Create vector between first and last point
    lineVec = allCoord(end,:) - firstPoint;

    % Normalize vector
    lineVecN = lineVec / sqrt(sum(lineVec.^2));

    % Find the distance from each point to the line, by splitting data into 
    % perpendicular to the line and transversal.
    vecFromFirst = bsxfun(@minus, allCoord, firstPoint);

    scalarProduct = dot(vecFromFirst, repmat(lineVecN,nPoints,1), 2);
    vecFromFirstParallel = scalarProduct * lineVecN;
    vecToLine = vecFromFirst - vecFromFirstParallel;

    % Distance to line is the norm of vecToLine
    distToLine = sqrt(sum(vecToLine.^2,2));

    % Plot the distance to the line
    % figure('Name','distance from curve to line'), plot(distToLine)

    %Find the maximum aka the best trade off point
    [maxDist,idxOfBestPoint] = max(distToLine);

    idxOfBestPointAll(i) = idxOfBestPoint;
    idxOfBestPointAll(i) = idxOfBestPoint;

end

% Plot contrast vs. kernel size with optimal kernel points
figure(2)
plot(kernelSize,contrast(:,:))
hold on

for i = 1:size(contrast,1)
    allCoord = [kernelSize;contrast(i,:)]';
    plot(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), 'or','HandleVisibility','off') 
    textString = sprintf('  [%.2f, %.2f]', allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2));
    text(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), textString, 'FontSize', 10, 'Color', 'k');
end
legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
ylabel('Contrast')
xlabel('Kernel size')
axis tight
grid on

saveas(figure(2),strcat('Contrast_vs_Kernel_size_',type,'Optimal.png'))