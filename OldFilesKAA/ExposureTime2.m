
clear all
clc

%% ------------- BEGIN CODE --------------
% Select data to work with
files = ["speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS8", ...
         "speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS8", ...
         "speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS8"];
%files = ["speckleBrow_c0007_spS1","speckleBrow_c0007_spS2","speckleBrow_c0007_spS4","speckleBrow_c0007_spS6","speckleBrow_c0007_spS8","speckleBrow_c0007_spS10","speckleOrd_c0066_spS1","speckleOrd_c0066_spS2","speckleOrd_c0066_spS4","speckleOrd_c0066_spS6","speckleOrd_c0066_spS8","speckleOrd_c0066_spS10", "speckleOrd_c0213_spS1","speckleOrd_c0213_spS2","speckleOrd_c0213_spS4","speckleOrd_c0213_spS6","speckleOrd_c0213_spS8","speckleOrd_c0213_spS10"];
filenames = ["Parenchyma spS2", "Parenchyma spS4","Parenchyma spS8", ...
         "MediumVessel spS2","MediumVessel spS4","MediumVessel spS8", ...
         "LargeVessel spS2","LargeVessel spS4","LargeVessel spS8"];
type = "PML248"; % state the included files 



%% Set exposure time 
exposure = 1000; % set exposure time [1/1000 mus]  

RUN = sprintf('%dmsExp%s',(exposure/1000),type); % This variable should not be changed 


% Generate [averaged over time] speckle images for all selected files
mImage = zeros(100,100,length(files));
for i = 1:1:length(files)
    load(files(i));
    mImage(:,:,i) = mean(speckle(:,:,1:exposure),3); % this is not what we want. we want cummulative sum??
    save(strcat('m',RUN),'mImage');
end

% Vizualize the averaged speckle pattern files
load(sprintf('m%s.mat',RUN)) 
figure('name',sprintf('m%s',RUN))
title(sprintf('m%s',RUN));
axis image
for i = 1:1:length(files)
    subplot(3,length(files)/3,i)
    imagesc(mImage(:,:,i)) 
    colormap(gray)
    title(filenames(i))
    sgtitle(sprintf('%dmsExp',(exposure/1000)))
    caxis([prctile(mImage(:),1),prctile(mImage(:),99)])
    saveas(gcf,sprintf('m%s',RUN),'fig') 
end


% Calculate contrast with getSLSCI function

% Vizualize the contrast
kernelSize=7;
load(sprintf('m%s.mat',RUN)) 
figure('name',sprintf('Contrast kernel%d mean%s',kernelSize,RUN))
title(sprintf('m%s',RUN));
axis image
K = zeros(51,51,length(files)); % change to calculate contrast over entire image
conVal = zeros(1,length(files));
for i = 1:1:length(files)
    subplot(3,length(files)/3,i)
    K(:,:,i) = mean(getSLSCI(mImage(25:75,25:75,i),kernelSize,'cpu','none'),3); % Contrast estimation from center pixels
    imagesc(K(:,:,i))
    colormap(gray)
    title(filenames(i))
    sgtitle(sprintf('Contrast %dmsExp',(exposure/1000)))
    caxis([prctile(K(:),1),prctile(K(:),99)])
    saveas(gcf,sprintf('CONTRASTm%s',RUN),'fig')
    
    %calculate contrast
    conVal(i) = mean(getSLSCI(mImage(25:75,25:75,i),kernelSize,'cpu','none'),[1 2]);
    savename = sprintf('ConVal%dmsExp',(exposure/1000));
    save(savename,'conVal','filenames','RUN');
end



%% Now plotting those contrast values 

exps = 1:1:5;
y_contrasts = zeros(length(exps),length(filenames));
for i=1:1:length(exps)
    load(sprintf('ConVal%dmsExp',exps(i)));
    for j=1:1:length(conVal)
        y_contrasts(i,j) = conVal(j);
    end 
end

%%

figure('name','contrast plot')
C = {[0.9290, 0.6940, 0.1250],[0.9290, 0.6940, 0.1250],[0.9290, 0.6940, 0.1250], ...
    [0.4940, 0.1840, 0.5560],[0.4940, 0.1840, 0.5560],[0.4940, 0.1840, 0.5560], ...
    [0.3010, 0.7450, 0.9330],[0.3010, 0.7450, 0.9330],[0.3010, 0.7450, 0.9330]};
Markers = {'o','s','*','o','s','*','o','s','*'};
for j = 1:1:(length(conVal))
    file=filenames(j);
    y = y_contrasts(:,j);
    plot(exps,y,'color',C{j},'marker',Markers{j},'DisplayName',file,'LineWidth',1)
    legend
    hold on
    xlim([0.9,5.1])
end
xlabel( 'Exposure time [10^3mum]');
ylabel( 'Contrast' );
grid on




%% ------------- END CODE --------------