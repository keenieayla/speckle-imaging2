%% Clean up
close all
clear all

%% Load Speckle Image

load('speckleBrowP')
load('speckleOrdM')
load('speckleOrdL')

%% Viusualize Speckle Pattern

% figure(1)
T = size(speckleOrdL,3);
title('Speckle Pattern')
axis image
for t = 1:1:T
    imagesc(speckleBrowP(:,:,t))   
    pause(.05)
end

%% Spatial Correlation

maxLag = 10;
[corr,lags] = getSpatialCorr(I_t(:,:,1),maxLag); % compute spatial autocorrelation
[~,locs,w,~]=findpeaks(corr); % find peaks and prominence
spSize=w(locs==maxLag+1); % speckle size = half width of lag 0 peak
[spSizeDmitry,corrDmitry] = getSpeckleSize(I_t(:,:,1),maxLag); % compare to dmitry's corr

% Plot
figure(2)
findpeaks(corr,lags,'Annotate','extents')
xlabel('\tau')
ylabel('Correlation')
legend('Correlation','Peaks','Prominence',sprintf('Speckle Size: %.2f',spSize));
title('Spatial Correlation')

%% Temporal Autocorrelaiton funciton g2
% Temporal autocovariance using edited function of Dmitry

maxLag = 50;
g2 = getG2(I_t,maxLag); % vector of temporal correlation 
decorrT = find(interp1(0:1:maxLag,g2,0:(1/100):maxLag) <= 1,1)/100; % decorrelation time

% Plot
figure(3)
plot(0:1:maxLag,g2,'--o');
yline(1,'k--');
xline(decorrT,'k--');
xlabel('\tau')
ylabel('Correlation')
legend('Correlation',sprintf('Decorr.-Time: %.2f',decorrT));
title('Temporal Correlation')