clear all; clc; close all

%% Load LSCI data
% located in Data folder

load('laserSpeckleData.mat') % read Dmitry's LSCI

%% Visualize LSCI data
% Only baseline (BL) image

imageBL = dataBL(:,:,1); % first LSCI image

% Plot
figure()
imagesc(imageBL)
caxis([prctile(imageBL(:),1), prctile(imageBL(:),99)])
colormap gray
colorbar

%% Find no. of pixels per speckle

maxLags = 20;
[spSize,corr] = getSpeckleSize(dataBL(:,:,1),maxLags);

% Speckle size is 2.49 pixels

%% Spatial contrast

% A. Calculate spatial contrast for kernels = 1:2:399
% Plot
figure()
kernelSize = 1:2:49;
contrast = zeros(1,length(kernelSize));
for idx = 1:1:length(kernelSize)
    contrast(:,idx) = mean(getSLSCI(dataBL(:,:,1),kernelSize(idx),'cpu','none'),[1,2]); % calculates contrast from center pixels
end
plot(kernelSize,contrast,'k-o')
axis tight
grid on
legend('Baseline data','Location','best')
ylabel('Spatial contrast')
xlabel('Kernel size')
set(gca,'FontSize',12)

% B. Calculate spatial contrast for kernel = 7
imageBLk = getSLSCI(dataBL(:,:,1),7,'cpu','none');
kvalue = mean(getSLSCI(dataBL(:,:,1),7,'cpu','none'),[1,2]); % contrast value

% Plot spatial contrast for kernel = 7
figure()
imagesc(imageBLk)
caxis([prctile(imageBLk(:),1), prctile(imageBLk(:),99)])
colormap gray
colorbar
title('Baseline')
set(gca,'FontSize',12)

%% Temporal contrast
% takes some time to compute

kernelSize = 3:2:49; % biggest kernel size is 999
tLSCI = zeros(1,length(kernelSize));
f = waitbar(0,sprintf('Calc. temporal contrast (%s)',kernelSize)); % shows status of simulation
figure()
    for j = 1:length(kernelSize)
        waitbar(j/length(kernelSize),f); % update waitbar
        tLSCI(:,j) = mean(getTLSCI(dataBL(:,:,:),kernelSize(j),'cpu','none'),[1,2,3]);
    end
delete(f)
plot(kernelSize,tLSCI,'-ko')
axis tight
grid on
legend('Baseline data','Location','best')
ylabel('Temporal contrast')
xlabel('Kernel size')
set(gca,'Xtick',3:2:49,'FontSize',12)
