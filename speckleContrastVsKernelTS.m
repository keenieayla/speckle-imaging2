clear all; close all; clc;

%% Add files to Matlab path

% Add data to path
addpath('Data/Speckles') % add path to Keenies new data files from dropbox
% Add functions to path
addpath('Functions') 

%% Create image of 5 ms

% Choose file and type
% files = ["speckleMed_spS001","speckleMed_spS01", "speckleMed_spS1","speckleMed_spS2","speckleMed_spS3","speckleMed_spS4","speckleMed_spS10","speckleMed_spS16"]; type = "OrdM";
% files = ["speckleLar_spS001","speckleLar_spS01", "speckleLar_spS1","speckleLar_spS2","speckleLar_spS3","speckleLar_spS4","speckleLar_spS10","speckleLar_spS16"]; type = "OrdL";
files = ["specklePar_spS001","specklePar_spS01", "specklePar_spS1","specklePar_spS2","specklePar_spS3","specklePar_spS4","specklePar_spS10","specklePar_spS16"]; type = "BrowP";

for i = 1:1:length(files)
    load(files(i));
    mImage(:,:,i) = mean(speckle(:,:,1:5000),3);
    
    % Saves averaged images according to motion type
    save(strcat('mData',type),'mImage');

end

%% Calculate Speckle Contrast vs. Kernel Size

% Choose 1 file to plot
% files = "mDataOrdM"; type = "OrdM";
% files = "mDataOrdL"; type = "OrdL";
files = "mDataBrowP"; type = "BrowP";

load(files);

kernelSize = 1:2:49;
contrast = zeros(8,length(kernelSize));
contrastImages = zeros(length(mImage), length(mImage), 8, length(kernelSize));
for k = 1:length(kernelSize)
    contrast(:, k) = mean(getSLSCI(mImage(:,:,:),kernelSize(k),'cpu','none'),[1,2]);% mean(getSLSCI(mImage(:,:,:),kernelSize(k),'cpu','none'),[1,2]);
    
    dim = size(mImage,3);
    for j = 1:dim
        contrastImages(:,:,j,k) = getSLSCI(mImage(:,:,j),kernelSize(k),'cpu','none');     
    end
end

spec = [0.5 0.8 1 2 3 4 10 16];
kernRatio2 = zeros(size(contrast));
for j=1:1:size(spec,2)
    kernRatio2(j,:) = (kernelSize)/spec(j);
end

kern = kernRatio2'; kern = kern(:)';
con = contrast'; con = con(:)'; rcon = con./max(con);

% sortedDataOrdMTS = ([kernelSize; contrast(1,:); contrast(2,:); contrast(3,:); contrast(4,:); contrast(5,:); contrast(6,:); contrast(7,:); contrast(8,:)]);
% sortedDataOrdLTS = ([kernelSize; contrast(1,:); contrast(2,:); contrast(3,:); contrast(4,:); contrast(5,:); contrast(6,:); contrast(7,:); contrast(8,:)]);
% sortedDataBrowPTS = ([kernelSize; contrast(1,:); contrast(2,:); contrast(3,:); contrast(4,:); contrast(5,:); contrast(6,:); contrast(7,:); contrast(8,:)]);
%% When the two above sections are run for all datatypes, continue ->

save('sortedDataTS.mat','sortedDataOrdLTS', 'sortedDataOrdMTS','sortedDataBrowPTS');

%% Remove border of images in relation to kernel size

contrastImagesCrop = contrastImages;
for k = 1:1:length(kernelSize) % kernel size length is 25 for a kernel size of 49!
    contrastImagesCrop(1:round(kernelSize(k)/2),:,:,k) = 0;
    contrastImagesCrop(end-round(kernelSize(k)/2):end,:,:,k) = 0;
    
    contrastImagesCrop(:,1:round(kernelSize(k)/2),:,k) = 0;
    contrastImagesCrop(:,end-round(kernelSize(k)/2):end,:,k) = 0;
end

% Visualize averaged images, kernelsize = 49
figure(1)
for i = 1:6 % length of files
    subplot(2,6,i)
    imagesc(contrastImages(:,:,i,end))
    axis image
    colormap gray
    colorbar
    minVal = min(min(min(min(contrastImages(:,:,:,:)))));
    maxVal = max(max(max(max(contrastImages(:,:,:,:)))));
    caxis([minVal maxVal]);
end
for i = 1:6 % length of files
    subplot(2,6,6+i)
    imagesc(contrastImagesCrop(:,:,i,end))
    colormap gray
    colorbar
    axis image
    caxis([minVal maxVal]);
end

%% Plot contrast vs. kernel size
figure(2)
plot(kernelSize,contrast)
axis tight
grid on
legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 3','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
ylabel('Contrast')
xlabel('Kernel size')
% saves figure and a .png file in directory according to motion type
% saveas(figure(2),strcat('Contrast_vs_Kernel_size_',type,'.png'))



%%
clc; clear all; close all;

%% Load files, calculate contrast (same as above), find optimal point and also save optimal points
% Choose 1 file to plot
% files = "mDataOrdM"; type = "OrdM";
% files = "mDataOrdL"; type = "OrdL";
% files = "mDataBrowP"; type = "BrowP";

load(files);

kernelSize = 1:2:49;
contrast = zeros(8,length(kernelSize));
contrastImages = zeros(length(mImage), length(mImage), 8, length(kernelSize));
for k = 1:length(kernelSize)
    contrast(:, k) = mean(getSLSCI(mImage(:,:,:),kernelSize(k),'cpu','none'),[1,2]);% mean(getSLSCI(mImage(:,:,:),kernelSize(k),'cpu','none'),[1,2]);
    
    dim = size(mImage,3);
    for j = 1:dim
        contrastImages(:,:,j,k) = getSLSCI(mImage(:,:,j),kernelSize(k),'cpu','none');     
    end
end

% nPoints = length(kernelSize(2:end));
nPoints = length(kernelSize);
for i = 1:size(contrast,1)
    allCoord = [kernelSize;contrast(i,:)]';
%    allCoord = allCoord(2:end,:); % <---- Use data for kernel 3..end

    % Pull out first point
    firstPoint = allCoord(1,:);

    % Create vector between first and last point
    lineVec = allCoord(end,:) - firstPoint;

    % Normalize vector
    lineVecN = lineVec / sqrt(sum(lineVec.^2));

    % Find the distance from each point to the line, by splitting data into 
    % perpendicular to the line and transversal.
    vecFromFirst = bsxfun(@minus, allCoord, firstPoint);

    scalarProduct = dot(vecFromFirst, repmat(lineVecN,nPoints,1), 2);
    vecFromFirstParallel = scalarProduct * lineVecN;
    vecToLine = vecFromFirst - vecFromFirstParallel;

    % Distance to line is the norm of vecToLine
    distToLine = sqrt(sum(vecToLine.^2,2));

    % Plot the distance to the line
    % figure('Name','distance from curve to line'), plot(distToLine)

    %Find the maximum aka the best trade off point
    [maxDist,idxOfBestPoint] = max(distToLine);

    idxOfBestPointAll(i) = idxOfBestPoint;
    idxOfBestPointAll(i) = idxOfBestPoint;
end

% Plot contrast vs. kernel size with optimal kernel points <---------------------------
% figure(3)
% plot(kernelSize,contrast(:,:))
% hold on
% for i = 1:size(contrast,1)
%     allCoord = [kernelSize;contrast(i,:)]';
% %    idxOfBestPointAll = idxOfBestPointAll+1;
%     plot(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), 'or','HandleVisibility','off') 
% %    textString = sprintf('  [%.2f, %.2f]', allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2));
% %    text(allCoord(idxOfBestPointAll(i),1), allCoord(idxOfBestPointAll(i),2), textString, 'FontSize', 10, 'Color', 'k');
% end
% legend('Speckle to pixel size ratio 1','Speckle to pixel size ratio 2','Speckle to pixel size ratio 3','Speckle to pixel size ratio 4','Speckle to pixel size ratio 6','Speckle to pixel size ratio 8','Speckle to pixel size ratio 10','Location','best')
% ylabel('Contrast')
% xlabel('Kernel size')
% axis tight
% grid on
% % saveas(figure(3),strcat('Contrast_vs_Kernel_size_',type,'Optimal.png'))


spec = [0.5 0.8 1 2 3 4 10 16]; spec = spec'; 
DataToSave = [spec, allCoord(idxOfBestPointAll(:),1)];
save( strcat('SpatOptSpecPoints',type,'.mat'),'DataToSave');

%% PLOT ALL DATA TOGETHER. Have sortedDataTS created first. 
% ------------- PLOT FOR PRESENTATION ----------------------------------------------
clc; clear all; close all; 
load('sortedDataTS.mat')
load('spatial_mean_std.mat')

spec = [0.5 0.8 1 2 3 4 10 16];
col = [ 'r' 'k' 'b' 'g'  'y' 'm' 'c' '-r'];

figure(2)
subplot(1,3,1)
for i = 2:length(spec)-1
plot(sortedDataBrowPTS(1,:)./spec(i),sortedDataBrowPTS(i+1,:), 'LineWidth',2)
hold on
end

plot([brow_mean brow_mean],[0 0.4], 'k', 'LineWidth',3)
h=fill([brow_mean-brow_std,brow_mean+brow_std,brow_mean+brow_std,brow_mean-brow_std],[0,0,0.4,0.4],'k');
h.FaceAlpha=0.3;
xlim([1 15])
ylim([0 0.4])
grid on
set(gca,'Xtick',1:2:50,'FontSize',16)
ylabel('Spatial contrast')
xlabel('Kernel to speckle size ratio')
% title(['Brow. Opt. kernel/speckle = ' num2str(brow_mean,'%4.1f')])
% hold off
str = ['Opt. kernel size ' num2str(brow_mean,'%4.1f')];
str2 = ['Std. of mean ' num2str(brow_std,'%4.1f')];
% hleg = legend('Kernel/Speckle < 1','Kernel/Speckle 1','Kernel/Speckle 2','Kernel/Speckle 3','Kernel/Speckle 4','Kernel/Speckle 10',str, str2, 'Location', 'best','FontSize',12);
% htitle = get(hleg,'Title');
%set(htitle,'String','Parenchyma')
set(gcf,'PaperUnits','centimeters','Position',[50,50,700/3,250])
% set(gcf,'PaperUnits','centimeters','PaperPosition',[1 1 7.2 5.0]) 
% set(gcf,'Units','centimeters','position',get(gcf,'PaperPosition'))
hold off

subplot(1,3,2)
for i = 2:length(spec)-1
plot(sortedDataOrdMTS(1,:)./spec(i),sortedDataOrdMTS(i+1,:), 'LineWidth',2)
hold on
end
plot([medium_mean medium_mean],[0 0.4], 'k', 'LineWidth',3)
h=fill([medium_mean-medium_std,medium_mean+medium_std,medium_mean+medium_std,medium_mean-medium_std],[0,0,0.4,0.4],'k');
h.FaceAlpha=0.3;
xlim([1 15])
ylim([0 0.2])
grid on
set(gca,'Xtick',1:2:50,'FontSize',16)
% ylabel('Spatial contrast')
xlabel('Kernel to speckle size ratio')
% title(['Medium. Opt. kernel/speckle = ' num2str(medium_mean,'%4.1f')])
% hold off
str = ['Opt. kernel size ' num2str(medium_mean,'%4.1f')];
str2 = ['Std. of mean ' num2str(medium_std,'%4.1f')];
% hleg = legend('Kernel/Speckle < 1','Kernel/Speckle 1','Kernel/Speckle 2','Kernel/Speckle 3','Kernel/Speckle 4','Kernel/Speckle 10',str, str2, 'Location', 'best','FontSize',12);
% htitle = get(hleg,'Title');
%set(htitle,'String','Medium')
% set(gca,'units','points','Position',[700/3+50,50,700/3,250])
% set(gcf, 'PaperUnits', 'centimeters');
hold off



subplot(1,3,3)
for i = 2:length(spec)-1
plot(sortedDataOrdLTS(1,:)./spec(i),sortedDataOrdLTS(i+1,:), 'LineWidth',2)
hold on
end
plot([large_mean large_mean],[0 0.4], 'k', 'LineWidth',3)
h=fill([large_mean-large_std,large_mean+large_std,large_mean+large_std,large_mean-large_std],[0,0,0.4,0.4],'k');
h.FaceAlpha=0.3;
xlim([1 15])
ylim([0 0.1])
grid on
set(gca,'Xtick',1:2:50,'FontSize',16)
% ylabel('Spatial contrast')
xlabel('Kernel to speckle size ratio')
%title(['Large. Opt. kernel/speckle = ' num2str(large_mean,'%4.1f')])
str = ['Opt. kernel size ' num2str(large_mean,'%4.1f')];
str2 = ['Std. of mean ' num2str(large_std,'%4.1f')];
% hleg = legend('Kernel/Speckle < 1','Kernel/Speckle 1','Kernel/Speckle 2','Kernel/Speckle 3','Kernel/Speckle 4','Kernel/Speckle 10',str, str2, 'Location', 'best','FontSize',12);
% htitle = get(hleg,'Title');
% set(htitle,'String','Large')
% set(gca,'units','points','Position',[700/3*2+50,50,700/3,250])
% set(gcf, 'PaperUnits', 'centimeters');
hold off

set(gcf, 'Position',  [100,100,1500,500])
saveas(gcf,'spat_results.png')


figure(3)
for i = 2:length(spec)-1
plot(sortedDataBrowPTS(1,:)./spec(i),sortedDataBrowPTS(i+1,:), 'LineWidth',2)
hold on
end

plot([brow_mean brow_mean],[0 0.35], 'k', 'LineWidth',3)
h=fill([brow_mean-brow_std,brow_mean+brow_std,brow_mean+brow_std,brow_mean-brow_std],[0,0,0.35,0.35],'k');
h.FaceAlpha=0.3;
xlim([0 25])
ylim([0.26 0.34])
grid on
set(gca,'Xtick',0:5:50,'FontSize',16)
ylabel('Spatial contrast')
xlabel('Kernel to speckle size ratio')
% title(['Brow. Opt. kernel/speckle = ' num2str(brow_mean,'%4.1f')])
% hold off
str = ['Opt. kernel size [' num2str(brow_mean,'%4.1f') ', ' num2str(medium_mean,'%4.1f') ', ' num2str(large_mean,'%4.1f') ']'];
str2 = ['Std. of mean [' num2str(brow_std,'%4.1f') ', ' num2str(medium_std,'%4.1f') ', ' num2str(large_std,'%4.1f') ']'];
hleg = legend('Speckle < 1','Speckle 1','Speckle 2','Speckle 3','Speckle 4','Speckle 10',str, str2, 'Location', 'northoutside','orientation','horizontal','FontSize',12);
% htitle = get(hleg,'Title');
% set(htitle,'String','Parenchyma')
set(gcf, 'Position',  [100,100,350,500])
hold off
saveas(gcf,'spat_result_small.png')
