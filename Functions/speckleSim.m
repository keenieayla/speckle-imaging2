%speckleSim - ...

%%------------- BEGIN CODE --------------%%

function I_t = speckleSim(motion,T,dt,sizeXY,particlesN,pixelsN,pixelSize,c)
rng(0);

%-------- Parameters -------- 

% Volume
sizeX = sizeXY; sizeY = sizeXY; % mum
sizeZ = 100;

% Particle positions 
particleX = rand(particlesN,1).*sizeX;
particleY = rand(particlesN,1).*sizeY;
particleZ = rand(particlesN,1).*sizeZ;

% Source
lambda = 0.785; % mum
k = 2*pi/lambda;

% Sensor
pixelsNx = pixelsN; pixelsNy = pixelsN; % 
sensorX = sizeX/2; sensorY = sizeY/2; sensorZ = sizeZ*10; %
pixelsPosX = sensorX+ones(pixelsNx,1)*((1:pixelsNx)-0.5*pixelsNx).*pixelSize;
pixelsPosY = sensorY+((1:pixelsNy)'-0.5*pixelsNy)*ones(1,pixelsNy).*pixelSize;

% Motion
motionType = motion;
switch motionType
    case  'ordered'
        std=c/10; % random std factor for ordered motion
        motionX = c + randn(particlesN,T)*std; % ordered motion in mum
%         motionY = zeros(particlesN,T); % mum
%         motionZ = zeros(particlesN,T); % mum
        % here we add random motion in y and z direction 
        motionY = randn(particlesN,T)*std; % mum
        motionZ = randn(particlesN,T)*std; % mum

    case 'brownian'
        motionX = randn(particlesN,T)*c; % random motion in mum
        motionY = randn(particlesN,T)*c; % mum
        motionZ = randn(particlesN,T)*c; % mum
end

%-------- Simulation -------- 
I_t = zeros(pixelsNx,pixelsNy,T);% Initialize the field

f = waitbar(0,sprintf('Simulating Speckles (%s, %f)',motion,c)); % shows status of simulation

for t = 1:dt:T
    waitbar(t/T,f); % update waitbar
    
    E = zeros(pixelsNx,pixelsNy);
    for i = 1:1:particlesN   
        r = sqrt(((particleX(i)-pixelsPosX).^2 + (particleY(i)-pixelsPosY).^2 + (particleZ(i)-sensorZ).^2));
        E = E+exp(1i*k*r-1i*k*t)./r;
    end
    I = E.*conj(E);
    I_t(:,:,t) = I./mean(I(:)); 
    
%     % Plot
%     subplot(1,2,1);
%     scatter3(particleX,particleY,particleZ,'.')
%     axis([-1 sizeX+1 -1 sizeY+1])
%     daspect([1 1 1])
%     xlabel('X position')
%     ylabel('Y position')
%     zlabel('Z position')
%     title(sprintf('Particle Position at t=%.0f', t))
%     
%     subplot(1,2,2);
%     histogram([particleX particleY particleZ],linspace(0,sizeX,10))
%     xlabel('Position')
%     ylabel('Frequency for XYZ dimensions')
%     ylim([0 particlesN/2])
%     title('Histogram')
%     axis square
%     
%     drawnow limitrate
       
    % Update position
    % Random and ordered motion
    particleX = particleX + dt*motionX(:,t);
    particleY = particleY + dt*motionY(:,t);
    particleZ = particleZ + dt*motionZ(:,t);

    % Introduce new particle if moved out of volume
    % Find particles who moved out
    idxX = particleX<0 | particleX>sizeX;
    idxY = particleY<0 | particleY>sizeY;
    idxZ = particleZ<0 | particleZ>sizeZ;
    
    if any(idxX)  % particle leaves the volume in x-direction
    particleX(idxX) = mod(particleX(idxX),sizeX); % opposite side
    particleY(idxX) = rand(sum(idxX),1).*sizeY; % y random
    particleZ(idxX) = rand(sum(idxX),1).*sizeZ; % z random    
    
    elseif any(idxY) % particle leaves the volume in y-direction
    particleY(idxY) = mod(particleY(idxY),sizeY); % opposite side
    particleX(idxY) = rand(sum(idxY),1).*sizeX; % x random
    particleZ(idxY) = rand(sum(idxY),1).*sizeZ; % z random    
    
    elseif any(idxZ) % particle leaves the volume in z-direction
    particleZ(idxZ) = mod(particleZ(idxZ),sizeZ); % opposite side
    particleY(idxZ) = rand(sum(idxZ),1).*sizeY; % y random
    particleX(idxZ) = rand(sum(idxZ),1).*sizeX; % x random
    end
end
delete(f)
end

%------------- END OF CODE --------------
% Comments: 