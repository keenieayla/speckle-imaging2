function [xP,yP,corr_int,steps] = getCorrVal(corr,legs,val)
%GETCORRVAL ouputs coordingates for a specify query point for a correlation
%using interpolation
%   corr = array correlation values
%   legs = array of leg indices
%   val = query value
stepSize = 0.001;
steps = 0:stepSize:max(legs);
corr_int = interp1(legs,corr,steps);
xP = steps(find(corr_int <= val,1));
yP = corr_int(find(corr_int <= val,1));
end

